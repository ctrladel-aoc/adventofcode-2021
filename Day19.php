<?php

namespace y2021;

use aoc\Utility\Grid;
use aoc\Utility\GridCoordinate;
use y2021\src\Day;
use function DeepCopy\deep_copy;

require __DIR__ . '/../../autoload.php';

class Day19 extends Day {

  protected const DAY = 19;

  public function __construct() {
    $this->addExample(1, 1, "--- scanner 0 ---\n-1,-1,1\n-2,-2,2\n-3,-3,3\n-2,-3,1\n5,6,-4\n8,0,7", "6");
    $this->addExample(1, 2, "--- scanner 0 ---\n404,-588,-901\n528,-643,409\n-838,591,734\n390,-675,-793\n-537,-823,-458\n-485,-357,347\n-345,-311,381\n-661,-816,-575\n-876,649,763\n-618,-824,-621\n553,345,-567\n474,580,667\n-447,-329,318\n-584,868,-557\n544,-627,-890\n564,392,-477\n455,729,728\n-892,524,684\n-689,845,-530\n423,-701,434\n7,-33,-71\n630,319,-379\n443,580,662\n-789,900,-551\n459,-707,401\n\n--- scanner 1 ---\n686,422,578\n605,423,415\n515,917,-361\n-336,658,858\n95,138,22\n-476,619,847\n-340,-569,-846\n567,-361,727\n-460,603,-452\n669,-402,600\n729,430,532\n-500,-761,534\n-322,571,750\n-466,-666,-811\n-429,-592,574\n-355,545,-477\n703,-491,-529\n-328,-685,520\n413,935,-424\n-391,539,-444\n586,-435,557\n-364,-763,-893\n807,-499,-711\n755,-354,-619\n553,889,-390\n\n--- scanner 2 ---\n649,640,665\n682,-795,504\n-784,533,-524\n-644,584,-595\n-588,-843,648\n-30,6,44\n-674,560,763\n500,723,-460\n609,671,-379\n-555,-800,653\n-675,-892,-343\n697,-426,-610\n578,704,681\n493,664,-388\n-671,-858,530\n-667,343,800\n571,-461,-707\n-138,-166,112\n-889,563,-600\n646,-828,498\n640,759,510\n-630,509,768\n-681,-892,-333\n673,-379,-804\n-742,-814,-386\n577,-820,562\n\n--- scanner 3 ---\n-589,542,597\n605,-692,669\n-500,565,-823\n-660,373,557\n-458,-679,-417\n-488,449,543\n-626,468,-788\n338,-750,-386\n528,-832,-391\n562,-778,733\n-938,-730,414\n543,643,-506\n-524,371,-870\n407,773,750\n-104,29,83\n378,-903,-323\n-778,-728,485\n426,699,580\n-438,-605,-362\n-469,-447,-387\n509,732,623\n647,635,-688\n-868,-804,481\n614,-800,639\n595,780,-596\n\n--- scanner 4 ---\n727,592,562\n-293,-554,779\n441,611,-461\n-714,465,-776\n-743,427,-804\n-660,-479,-426\n832,-632,460\n927,-485,-438\n408,393,-506\n466,436,-512\n110,16,151\n-258,-428,682\n-393,719,612\n-211,-452,876\n808,-476,-593\n-575,615,604\n-485,667,467\n-680,325,-822\n-627,-443,-432\n872,-547,-609\n833,512,582\n807,604,487\n839,-516,451\n891,-625,532\n-652,-548,-490\n30,-46,-14", "79");
    $this->addExample(2, 1, "--- scanner 0 ---\n404,-588,-901\n528,-643,409\n-838,591,734\n390,-675,-793\n-537,-823,-458\n-485,-357,347\n-345,-311,381\n-661,-816,-575\n-876,649,763\n-618,-824,-621\n553,345,-567\n474,580,667\n-447,-329,318\n-584,868,-557\n544,-627,-890\n564,392,-477\n455,729,728\n-892,524,684\n-689,845,-530\n423,-701,434\n7,-33,-71\n630,319,-379\n443,580,662\n-789,900,-551\n459,-707,401\n\n--- scanner 1 ---\n686,422,578\n605,423,415\n515,917,-361\n-336,658,858\n95,138,22\n-476,619,847\n-340,-569,-846\n567,-361,727\n-460,603,-452\n669,-402,600\n729,430,532\n-500,-761,534\n-322,571,750\n-466,-666,-811\n-429,-592,574\n-355,545,-477\n703,-491,-529\n-328,-685,520\n413,935,-424\n-391,539,-444\n586,-435,557\n-364,-763,-893\n807,-499,-711\n755,-354,-619\n553,889,-390\n\n--- scanner 2 ---\n649,640,665\n682,-795,504\n-784,533,-524\n-644,584,-595\n-588,-843,648\n-30,6,44\n-674,560,763\n500,723,-460\n609,671,-379\n-555,-800,653\n-675,-892,-343\n697,-426,-610\n578,704,681\n493,664,-388\n-671,-858,530\n-667,343,800\n571,-461,-707\n-138,-166,112\n-889,563,-600\n646,-828,498\n640,759,510\n-630,509,768\n-681,-892,-333\n673,-379,-804\n-742,-814,-386\n577,-820,562\n\n--- scanner 3 ---\n-589,542,597\n605,-692,669\n-500,565,-823\n-660,373,557\n-458,-679,-417\n-488,449,543\n-626,468,-788\n338,-750,-386\n528,-832,-391\n562,-778,733\n-938,-730,414\n543,643,-506\n-524,371,-870\n407,773,750\n-104,29,83\n378,-903,-323\n-778,-728,485\n426,699,580\n-438,-605,-362\n-469,-447,-387\n509,732,623\n647,635,-688\n-868,-804,481\n614,-800,639\n595,780,-596\n\n--- scanner 4 ---\n727,592,562\n-293,-554,779\n441,611,-461\n-714,465,-776\n-743,427,-804\n-660,-479,-426\n832,-632,460\n927,-485,-438\n408,393,-506\n466,436,-512\n110,16,151\n-258,-428,682\n-393,719,612\n-211,-452,876\n808,-476,-593\n-575,615,604\n-485,667,467\n-680,325,-822\n-627,-443,-432\n872,-547,-609\n833,512,582\n807,604,487\n839,-516,451\n891,-625,532\n-652,-548,-490\n30,-46,-14", "3621");
  }

  public function processInputs(array $inputs): array {

    /** @var \aoc\Utility\Grid[] $beacons */
    $beacons = [];
    $scanner = 0;
    foreach ($inputs as $input) {
      if (strpos($input, '--- scanner ') !== FALSE) {
       [$one, $two, $scanner, $three] = explode(' ', $input);
       $beacons["s$scanner"] = new Grid();
       continue;
      }

      if ($input === '') {
        continue;
      }

      [$x, $y, $z] = explode(',', $input);
      $beacon = new GridCoordinate(
        $x,
        $y,
        $z,
      );

      $beacons["s$scanner"]->addPoint($beacon);
    }

    return $beacons;
  }

  public function getAnswerPart1() {
    $inputs = $this->getInputs();

    $map = $this->mapScanners($inputs);

    fwrite(STDERR, "\n");

    $answer = count($map['beacons']);
    echo "\nAnswer: $answer";
    return $answer;
  }

  public function mapScanners($inputs) {
    $scanners = [];

    $scanner0 = array_shift($inputs);
    $scanners["s0"] = [
      'grid' => $scanner0,
      'points' => array_keys($scanner0->getPoints()),
      'offset' => ['s0' => [0,0,0]],
      'absolute' => ['offset' => [0,0,0], 'points' => $scanner0->toArray()],
      'overlap' => [],
    ];

    $allBeacons = $scanner0->toArray();

    $totalScanners = count($inputs);

    foreach ($inputs as &$input) {
      $rotations = $this->getRotations($input);
      $input = [
        'original' => $input,
        'rotations' => $rotations,
      ];
    }

    do {
      foreach ($inputs as $scanner => $beacons) {
        foreach ($beacons['rotations'] as $rotation) {
          foreach ($scanners as $k => $known) {
            $offset = $this->offsetPointChecker($known['grid'], $rotation);

            if ($offset === FALSE) {
              continue;
            }

            $offsetRotation = deep_copy($rotation);
            $offsetRotation->offsetGrid($offset->getX(), $offset->getY(), $offset->getZ());

            $offPoints = $offsetRotation->getPoints();

            if (count(array_intersect($offPoints, $known['points'])) >= 12) {
              $scanners[$scanner] = [
                'grid' => $offsetRotation,
                'points' => array_keys($offsetRotation->getPoints()),
                'offset' => [$k => [$offset->getX(), $offset->getY(), $offset->getZ()]],
                'overlap' => [$k],
              ];

              if ($k === 's0' && !isset($scanners[$scanner]['absolute'])) {
                $scanners[$scanner]['absolute'] = [
                  'offset' => [
                    $offset->getX(),
                    $offset->getY(),
                    $offset->getZ()
                  ],
                  'points' => $offPoints,
                ];

                $allBeacons = array_merge($allBeacons, $offPoints);
              }
              elseif(isset($scanners[$k]['absolute'])  && !isset($scanners[$scanner]['absolute'])) {
                $offset = [
                  $scanners[$k]['absolute']['offset'][0] + $offset->getX(),
                  $scanners[$k]['absolute']['offset'][1] + $offset->getY(),
                  $scanners[$k]['absolute']['offset'][2] + $offset->getZ(),
                ];

                $absoluteOffset = deep_copy($rotation);
                $absoluteOffset->offsetGrid($offset[0], $offset[1], $offset[2]);

                $scanners[$scanner]['absolute'] = [
                  'offset' => $offset,
                  'points' => $absoluteOffset->getPoints(),
                ];

                $allBeacons = array_merge($allBeacons, $absoluteOffset->getPoints());
              }

              $scanners[$k]['overlap'][] = $scanner;
              unset($inputs[$scanner]);
              fwrite(STDERR, "Matched Scanner: $scanner\nScanners matched: " . count($scanners) . "/$totalScanners\n");

              break 2;
            }
          }
        }
      }
    } while($inputs);

    return [
      'scanners' => $scanners,
      'beacons' => $allBeacons,
    ];
  }

  public function offsetPointChecker(Grid $source, Grid $grid2) {
    foreach ($source->getPoints() as $point1) {
      foreach ($grid2->getPoints() as $point2) {
        $offset = $point2->difference($point1);
        $offsetGrid2 = deep_copy($grid2);
        $offsetGrid2->offsetGrid($offset->getX(), $offset->getY(), $offset->getZ());

        $matched = 0;
        $sourcePoints = $source->getPoints();
        foreach ($offsetGrid2->getPoints() as $coord => $point) {
          if (isset($sourcePoints[$coord])) {
            $matched++;
          };

          if ($matched >= 12) {
            return $offset;
          }
        }
      }
    }

    return FALSE;
  }

  public function offsetThruRange($grid, $check, $axis, $min, $max, $offsets = []) {
    $list = range($min, $max);
    while($list) {
      $rand = array_rand($list);
      $o = $list[$rand];
      unset($list[$rand]);

      if ($axis === 'x') {
        $off = $this->offsetGridArray($grid, $o, 0, 0);
        $index = 0;
      }

      if ($axis === 'y') {
        $ox = $offsets[0] ?? 0;
        $off = $this->offsetGridArray($grid, $ox, $o, 0);
        $index = 1;
      }

      if ($axis === 'z') {
        $ox = $offsets[0] ?? 0;
        $oy = $offsets[1] ?? 0;
        $off = $this->offsetGridArray($grid, $ox, $oy, $o);
        $index = 2;
      }

      $matched = 0;
      $checkP = $check;
      foreach ($off as $point) {
        if (($search = array_search($point[$index], $checkP)) !== FALSE) {
          unset($checkP[$search]);
          $matched++;
        };
      }

      if ($matched >= 12) {
        return $o;
      }
    }

    return FALSE;
  }

  public function offsetGridArray($grid, $x, $y, $z) {
    $result = [];
    foreach ($grid as $point) {
      $point[0] += $x;
      $point[1] += $y;
      $point[2] += $z;

      $result["x" . $point[0] . "y" . $point[1] ."z" . $point[2]] = $point;
    }
    return $result;
  }

  public function getRotations(Grid $grid) {
    $rotations = [$grid];
    for ($flip = 0; $flip < 2; $flip++) {
      for ($rttt = 0; $rttt < 3; $rttt++) {
        foreach ($grid->getPoints() as $point) {
          $this->roll($grid, $point);
        }
        $rotations[] = deep_copy($grid);

        for ($t = 0; $t < 3; $t++) {
          foreach ($grid->getPoints() as $point) {
            $this->turn($grid, $point);
          }
          $rotations[] = deep_copy($grid);
        }
      }
      foreach ($grid->getPoints() as $point) {
        $this->roll($grid, $point);
      }
      foreach ($grid->getPoints() as $point) {
        $this->turn($grid, $point);
      }
      foreach ($grid->getPoints() as $point) {
        $this->roll($grid, $point);
      }
      $rotations[] = deep_copy($grid);
    }

    return $rotations;
  }

  public function roll(Grid $grid, GridCoordinate $point) {
    $x = $point->getX();
    $y = $point->getZ();
    $z = $point->getY() * -1;
    $newPoint = new GridCoordinate($x, $y, $z);
    $grid->movePoint($newPoint, $point);
  }

  public function turn(Grid $grid, GridCoordinate $point) {
    $x = $point->getY() * -1;
    $y = $point->getX();
    $z = $point->getZ();
    $newPoint = new GridCoordinate($x, $y, $z);

    $grid->movePoint($newPoint, $point);
  }

  public function getAnswerPart2() {
    $inputs = $this->getInputs();

    $map = $this->mapScanners($inputs);
    $grid = new Grid();

    $max = 0;
    foreach ($map['scanners'] as $scanner => $details) {
      foreach ($map['scanners'] as $scanner2 => $details2) {
        if ($scanner === $scanner2) {
          continue;
        }
        $p1 = new GridCoordinate($details['absolute']['offset'][0], $details['absolute']['offset'][1], $details['absolute']['offset'][2]);
        $p2 = new GridCoordinate($details2['absolute']['offset'][0], $details2['absolute']['offset'][1], $details2['absolute']['offset'][2]);

        $dist = $grid->getManhattanDistance($p1, $p2);

        if ($dist > $max) {
          $max = $dist;
        }
      }
    }

    $answer = $max;
    echo "\nAnswer: $answer";
    return $answer;
  }

}
