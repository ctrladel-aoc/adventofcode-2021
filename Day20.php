<?php

namespace y2021;

use aoc\Utility\Grid;
use aoc\Utility\GridCoordinate;
use y2021\src\Day;

require __DIR__ . '/../../autoload.php';

class Day20 extends Day {

  protected const DAY = 20;

  public function __construct() {
    $this->addExample(1, 1, "..#.#..#####.#.#.#.###.##.....###.##.#..###.####..#####..#....#..#..##..###..######.###...####..#..#####..##..#.#####...##.#.#..#.##..#.#......#.###.######.###.####...#.##.##..#..#..#####.....#.#....###..#.##......#.....#..#..#..##..#...##.######.####.####.#.#...#.......#..#.#.#...####.##.#......#..#...##.#.##..#...##.#.##..###.#......#.#.......#.#.#.####.###.##...#.....####.#..#..#.##.#....##..#.####....##...##..#...#......#.#.......#.......##..####..#...#.#.#...##..#.#..###..#####........#..####......#..#\n\n#..#.\n#....\n##..#\n..#..\n..###", "35");
    $this->addExample(2, 1, "..#.#..#####.#.#.#.###.##.....###.##.#..###.####..#####..#....#..#..##..###..######.###...####..#..#####..##..#.#####...##.#.#..#.##..#.#......#.###.######.###.####...#.##.##..#..#..#####.....#.#....###..#.##......#.....#..#..#..##..#...##.######.####.####.#.#...#.......#..#.#.#...####.##.#......#..#...##.#.##..#...##.#.##..###.#......#.#.......#.#.#.####.###.##...#.....####.#..#..#.##.#....##..#.####....##...##..#...#......#.#.......#.......##..####..#...#.#.#...##..#.#..###..#####........#..####......#..#\n\n#..#.\n#....\n##..#\n..#..\n..###", "3351");
  }

  public function processInputs(array $inputs): array {

    $algorithm = array_shift($inputs);
    array_shift($inputs);

    $image = new Grid();
    foreach ($inputs as $y => $input) {
      foreach (str_split($input) as $x => $value) {
        $point = new GridCoordinate($x, $y, meta:['value' => $value]);
        $image->addPoint($point);
      }
    }

    return [
      'algorithm' => $algorithm,
      'image' => $image,
    ];
  }

  public function getAnswerPart1() {
    $inputs = $this->getInputs();

    $algorithm = $inputs['algorithm'];
    /** @var Grid $image */
    $image = $inputs['image'];

    $odd = $algorithm[0];
    $o = str_repeat($odd, 9);
    $num = str_replace('.', '0', $o);
    $num = str_replace('#', '1', $o);
    $num = bindec($num);

    $even = substr($algorithm, $num, 1);
    for ($i = 0; $i < 2; $i++) {
      if ($i%2 === 0) {
        $offGrid = $even;
      }
      else {
        $offGrid = $odd;
      }
      $image = $this->enhanceImage($algorithm, $image, $offGrid);
    }

    $lit = 0;
    foreach ($image->getPoints() as $pixel) {
      if ($pixel->getMetaProperty('value') === '#') {
        $lit++;
      }
    }

    $answer = $lit;
    echo "\nAnswer: $answer";
    return $answer;
  }

  public function enhanceImage($algorithm, Grid $image, $offGridValue = '') {

    $pixels = $image->getPoints();

    $output = new Grid();
    foreach ($pixels as $pixel) {
      $newValue = $this->calcNewValue($image, $algorithm, $pixel, $offGridValue);
      $outputPixel = new GridCoordinate($pixel->getX(), $pixel->getY(), meta: ['value' => $newValue]);
      $output->addPoint($outputPixel);
    }

    $expand = 2;
    $bounds = $image->findBoundingBox();
    $outputPixels = $output->getPoints();
    for ($x = $bounds['min']->getX() - $expand; $x <= $bounds['max']->getX() + $expand; $x++) {
      for ($y = $bounds['min']->getY() - $expand; $y <= $bounds['max']->getY() + $expand; $y++) {
        if (isset($outputPixels["x$x" . "y$y" . "z0"])) {
          continue;
        }

        $newPixel = new GridCoordinate($x, $y, meta: ['value' => $offGridValue]);
        $value = $this->calcNewValue($image, $algorithm, $newPixel, $offGridValue);

        if ($value !== FALSE) {
          $newPixel->setMetaProperty('value', $value);
          $output->addPoint($newPixel);
        }
      }
    }

    return $output;
  }

  public function calcNewValue(Grid $image, $algorithm, GridCoordinate $pixel, $offGridValue) {
    $adj = $image->getAdjacentModifiers();
    $pixels= $image->getPoints();

    $lines = [];
    $count = 0;
    foreach ($adj as $a) {
      $x = $a[0] + $pixel->getX();
      $y = $a[1] + $pixel->getY();
      if (!isset($pixels["x$x" . "y$y" . "z0"])) {
        $value = $offGridValue;
      }
      else {
        $value = $pixels["x$x" . "y$y" . "z0"]->getMetaProperty('value');
        $count++;
      }

      if (!isset($lines[$y])) {
        $lines[$y] = [];
      }

      $lines[$y][$x] = $value;
      ksort($lines[$y]);
    }

    if ($count === 0) {
      return FALSE;
    }

    $lines[$pixel->getY()][$pixel->getX()] = $pixel->getMetaProperty('value');
    ksort($lines[$pixel->getY()]);

    ksort($lines);

    foreach ($lines as &$line) {
      $line = implode('', $line);
    }

    $num = implode('', $lines);
    $num = str_replace('.', '0', $num);
    $num = str_replace('#', '1', $num);
    $num = bindec($num);

    return $algorithm[$num];
  }

  public function getAnswerPart2() {
    $inputs = $this->getInputs();

    $algorithm = $inputs['algorithm'];
    /** @var Grid $image */
    $image = $inputs['image'];

    $odd = $algorithm[0];
    $o = str_repeat($odd, 9);
    $num = str_replace('.', '0', $o);
    $num = str_replace('#', '1', $num);
    $num = bindec($num);

    $even = substr($algorithm, $num, 1);
    for ($i = 0; $i < 50; $i++) {
      if ($i%2 === 0) {
        $offGrid = $even;
      }
      else {
        $offGrid = $odd;
      }
      $image = $this->enhanceImage($algorithm, $image, $offGrid);
    }

    $lit = 0;
    foreach ($image->getPoints() as $pixel) {
      if ($pixel->getMetaProperty('value') === '#') {
        $lit++;
      }
    }

    $answer = $lit;
    echo "\nAnswer: $answer";
    return $answer;
  }

}
