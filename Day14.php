<?php

namespace y2021;

use y2021\src\Day;

require __DIR__ . '/../../autoload.php';

class Day14 extends Day {

  protected const DAY = 14;

  public function __construct() {
    $this->addExample(1, 1, "NNCB\n\nCH -> B\nHH -> N\nCB -> H\nNH -> C\nHB -> C\nHC -> B\nHN -> C\nNN -> C\nBH -> H\nNC -> B\nNB -> B\nBN -> B\nBB -> N\nBC -> B\nCC -> N\nCN -> C", "1588");
    $this->addExample(2, 1, "NNCB\n\nCH -> B\nHH -> N\nCB -> H\nNH -> C\nHB -> C\nHC -> B\nHN -> C\nNN -> C\nBH -> H\nNC -> B\nNB -> B\nBN -> B\nBB -> N\nBC -> B\nCC -> N\nCN -> C", "2188189693529");
  }

  public function processInputs(array $inputs): array {

    $template = array_shift($inputs);
    $rules = [];
    foreach ($inputs as $input) {
      if ($input === '') {
        continue;
      }
      [$pattern, $insert] = explode(' -> ', $input);
      $rules[$pattern] = $insert;
    }

    $nextPairs = [];
    foreach ($rules as $pattern => $insert) {
      [$f, $s] = str_split($pattern);
      $nextPairs[$pattern] = [$f . $insert, $insert . $s];
    }

    $counts = [];
    for ($i = 0; $i < strlen($template) - 1; $i++) {
      $pair = substr($template, $i, 2);

      if (!isset($counts[$pair])) {
        $counts[$pair] = 0;
      }

      $counts[$pair]++;
    }

    return [
      'counts' => $counts,
      'next' => $nextPairs,
    ];
  }

  public function getAnswerPart1() {
    $inputs = $this->getInputs();

    $counts = $inputs['counts'];
    $next = $inputs['next'];


    for ($s = 0; $s < 10; $s++) {
      $counts = $this->replace($counts, $next);
    }

    $charCount = $this->countChars($counts);

    $min = min($charCount);
    $max = max($charCount);

    $answer = $max - $min;
    echo "\nAnswer: $answer";
    return $answer;
  }

  public function replace($current, $next) {
    $counts = [];
    foreach ($current as $p => $value) {
      foreach ($next[$p] as $n) {
        $counts[$n] = ($counts[$n] ?? 0) + $value;
      }
    }
    return $counts;
  }

  public function countChars($counts) {
    $charCount = [];
    foreach ($counts as $pair => $count) {
      [$f, $s] = str_split($pair);

      $charCount[$s] = ($charCount[$s] ?? 0) + $count;
    }

    return $charCount;
  }

  public function getAnswerPart2() {
    $inputs = $this->getInputs();

    $counts = $inputs['counts'];
    $next = $inputs['next'];

    for ($s = 0; $s < 40; $s++) {
      $counts = $this->replace($counts, $next);
    }

    $charCount = $this->countChars($counts);

    $min = min($charCount);
    $max = max($charCount);

    $answer = $max - $min;
    echo "\nAnswer: $answer";
    return $answer;
  }

}
