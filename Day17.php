<?php

namespace y2021;

use aoc\Utility\GridCoordinate;
use y2021\src\Day;

require __DIR__ . '/../../autoload.php';

class Day17 extends Day {

  protected const DAY = 17;

  public function __construct() {
    $this->addExample(1, 1, "target area: x=20..30, y=-10..-5", "45");
    $this->addExample(2, 1, "target area: x=20..30, y=-10..-5", "112");
  }

  public function processInputs(array $inputs): array {

    $inputs = reset($inputs);
    $inputs = str_replace('target area: ', '', $inputs);
    [$x, $y] = explode(', ', $inputs);
    $x = str_replace('x=', '', $x);
    $y = str_replace('y=', '', $y);
    [$xS, $xE] = explode('..', $x);
    [$yE, $yS] = explode('..', $y);

    return [
      'x' => ['start' => $xS, 'end' => $xE],
      'y' => ['start' => $yS, 'end' => $yE],
    ];
  }

  public function getAnswerPart1() {
    $inputs = $this->getInputs();

    $minBound = new GridCoordinate($inputs['x']['start'], $inputs['y']['start']);
    $maxBound = new GridCoordinate($inputs['x']['end'], $inputs['y']['end']);

    $answer = $this->findTrajectories($minBound, $maxBound, 'maxY');;
    echo "\nAnswer: $answer";
    return $answer;
  }

  public function findTrajectories($targetMin, $targetMax, $find = '') {
    $hits = [];
    $maxY = 0;
    for ($initialXV = 0; $initialXV <= $targetMax->getX(); $initialXV++) {
      for ($initialYV = $targetMax->getY() ; $initialYV <= abs($targetMax->getY()); $initialYV++) {
        $step = 0;
        $x = 0;
        $y = 0;
        $mY = 0;
        do {
          if ($x > 0) {
            $xD = -1;
          }
          elseif ($x < 0) {
            $xD = 1;
          }
          else {
            $xD = 0;
          }

          $xV = $initialXV + $xD * $step;
          $xV = $xV < 0 ? 0 : $xV;
          $x = $x + $xV;

          $yV = $initialYV - $step;
          $y = $y + $yV;

          if ($y > $mY) {
            $mY = $y;
          }

          $point = new GridCoordinate($x, $y);
          $relative = $this->relativeToTargetArea($point, $targetMin, $targetMax);

          if ($relative === 'hit') {
            $hits['x' . $initialXV . 'y' . $initialYV] = [$initialXV, $initialYV];

            if ($mY > $maxY) {
              $maxY = $mY;
            }
          }

          $step++;
        } while($relative === 'before');
      }
    }

    if ($find === 'maxY') {
      return $maxY;
    }

    return $hits;
  }

  public function relativeToTargetArea(GridCoordinate $point, GridCoordinate $minBound, GridCoordinate $maxBound) {
    $minX = $minBound->getX();
    $maxX = $maxBound->getX();
    if ($point->getX() > $maxX) {
      $xResult = 'past';
    }
    elseif ($point->getX() < $minX) {
      $xResult = 'before';
    }
    else {
      $xResult = 'hit';
    }

    $minY = $minBound->getY();
    $maxY = $maxBound->getY();
    if ($point->getY() > $minY) {
      $yResult = 'before';
    }
    elseif ($point->getY() < $maxY) {
      $yResult = 'past';
    }
    else {
      $yResult = 'hit';
    }

    if ($yResult === 'hit' && $xResult === 'hit') {
      return 'hit';
    }

    if (($yResult === 'before' || $yResult === 'hit') && ($xResult === 'before' || $xResult === 'hit')) {
      return 'before';
    }

    return 'past';
  }

  public function getAnswerPart2() {
    $inputs = $this->getInputs();

    $minBound = new GridCoordinate($inputs['x']['start'], $inputs['y']['start']);
    $maxBound = new GridCoordinate($inputs['x']['end'], $inputs['y']['end']);

    $hits = $this->findTrajectories($minBound, $maxBound);

    $answer = count($hits);
    echo "\nAnswer: $answer";
    return $answer;
  }

}
