<?php

namespace tests2021;

use y2021\Day20 as Day;
use y2021\src\DayInterface;

final class Day20Test extends DayTestBase {

  protected function setUp(): void {
    $this->day = new Day();
  }

  protected static function getDay(): DayInterface {
    return new Day();
  }

}