<?php

namespace tests2021;

use y2021\Day19 as Day;
use y2021\src\DayInterface;
use y2021\src\ExampleInterface;

final class Day19Test extends DayTestBase {

  protected function setUp(): void {
    $this->day = new Day();
  }

  protected static function getDay(): DayInterface {
    return new Day();
  }

  /**
   * @dataProvider example1Provider
   */
  public function testExample1(ExampleInterface $example) {
    echo "\nExample " . $example->getNumber() . ':';
    $this->day->setInputs($example->getInput());
    $answer = $this->day->getAnswerPart1(...$example->getArgs());

    $this->assertEquals($example->getAnswer(), $answer);
  }

  /**
   * @dataProvider part1Provider
   */
  public function testPart1($expected) {
    echo "\nPart 1:";
    $answer = $this->day->getAnswerPart1();
    $this->assertEquals($expected, $answer);
  }

  /**
   * @dataProvider example2Provider
   */
  public function testExample2(ExampleInterface $example) {
    echo "\nExample " . $example->getNumber() . ':';
    $this->day->setInputs($example->getInput());
    $answer = $this->day->getAnswerPart2(...$example->getArgs());

    $this->assertEquals($example->getAnswer(), $answer);
  }

  /**
   * @dataProvider part2Provider
   */
  public function testPart2($expected) {
    echo "\nPart 2:";
    $answer = $this->day->getAnswerPart2();
    $this->assertEquals($expected, $answer);
  }


}