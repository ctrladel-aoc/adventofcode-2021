<?php

namespace tests2021;

use y2021\Day12 as Day;
use y2021\src\DayInterface;

final class Day12Test extends DayTestBase {

  protected function setUp(): void {
    $this->day = new Day();
  }

  protected static function getDay(): DayInterface {
    return new Day();
  }

}