<?php

namespace y2021;

use aoc\Utility\Grid;
use aoc\Utility\GridCoordinate;
use y2021\src\Day;
use function DeepCopy\deep_copy;

require __DIR__ . '/../../autoload.php';

class Day25 extends Day {

  protected const DAY = 25;

  public function __construct() {
//    $this->addExample(1, 1, "...>...\n.......\n......>\nv.....>\n......>\n.......\n..vvv..", "58");
    $this->addExample(1, 2, "v...>>.vv>\n.vv>>.vv..\n>>.>v>...v\n>>v>>.>.v.\nv>v.vv.v..\n>.>>..v...\n.vv..>.>v.\nv.v..>>v.v\n....v..v.>", "58");
    $this->addExample(2, 1, "", "");
  }

  public function processInputs(array $inputs): array {

    $grid = new Grid();
    $grid = [];
    foreach ($inputs as $y => $input) {
      $grid[$y] = str_split($input);
    }

    return $grid;
  }

  public function getAnswerPart12() {
    $inputs = $this->getInputs();

    $moves = 0;
    $moved = FALSE;
    $state = $inputs;
    do {
      $moved = $state;
      foreach ($state as $y => $row) {
        foreach ($row as $x => $value) {
          if ($value === '.') {
            continue;
          }

          if ($value === '>') {
            if (!isset($state[$y][$x]) && $state[$y][0] === '.') {
              $moved[$y][$x] = '.';
              $moved[$y][0] = $value;
              $roll = $start->getPoints()['x0' . 'y' . $point->getY() . 'z0'];
              if ($roll->getMetaProperty('c') === '.') {
                $new = new GridCoordinate(0, $point->getY(), meta: $point->getMeta());
                $moved->movePoint($new, $point);

                $new = new GridCoordinate($point->getX(), $point->getY(), meta: ['c' => '.']);
                $moved->movePoint($new, $point);
              }
            }
          }
          $c = $point->getMetaProperty('c');
          if ($c === '>') {
            $adj = $start->getAdjacentPoints($point, FALSE);

            if (!isset($adj['e'])) {
              $roll = $start->getPoints()['x0' . 'y' . $point->getY() . 'z0'];
              if ($roll->getMetaProperty('c') === '.') {
                $new = new GridCoordinate(0, $point->getY(), meta: $point->getMeta());
                $moved->movePoint($new, $point);

                $new = new GridCoordinate($point->getX(), $point->getY(), meta: ['c' => '.']);
                $moved->movePoint($new, $point);
              }
            }

            if (isset($adj['e']) && $adj['e']->getMetaProperty('c') === '.') {
              $new = new GridCoordinate($point->getX() + 1, $point->getY(), meta: $point->getMeta());
              $moved->movePoint($new, $point);

              $new = new GridCoordinate($point->getX(), $point->getY(), meta: ['c' => '.']);
              $moved->movePoint($new, $point);
            }
          }
        }

        $start = deep_copy($moved);

        foreach ($start->getPoints() as $point) {
          $c = $point->getMetaProperty('c');
          if ($c === 'v') {
            $adj = $start->getAdjacentPoints($point, FALSE);

            if (!isset($adj['n'])) {
              $roll = $start->getPoints()['x' . $point->getX() . 'y0z0'];
              if ($roll->getMetaProperty('c') === '.') {
                $new = new GridCoordinate($point->getX(), 0, meta: $point->getMeta());
                $moved->movePoint($new, $point);

                $new = new GridCoordinate($point->getX(), $point->getY(), meta: ['c' => '.']);
                $moved->movePoint($new, $point);
              }
            }

            if (isset($adj['n']) && $adj['n']->getMetaProperty('c') === '.') {
              $new = new GridCoordinate($point->getX(), $point->getY() + 1, meta: $point->getMeta());
              $moved->movePoint($new, $point);

              $new = new GridCoordinate($point->getX(), $point->getY(), meta: ['c' => '.']);
              $moved->movePoint($new, $point);
            }
          }
        }
      }
    } while (implode('' , $grid->printMeta(metaProp: 'c')) !== $initial);

    //    echo "\n" . implode('' , $grid->printMeta(metaProp: 'c')) . "\n";

    $moves = 0;
    do {
      $initial = implode('' , $grid->printMeta(metaProp: 'c'));
      $grid = $this->move($grid);
      $moves++;

      fwrite(STDERR, "\n$moves");
      //      echo "\n ----- Step $moves -----";
      //      echo "\n" . implode('' , $grid->printMeta(metaProp: 'c')) . "\n";

    } while (implode('' , $grid->printMeta(metaProp: 'c')) !== $initial);

    $answer = $moves;
    echo "\nAnswer: $answer";
    return $answer;
  }

  public function getAnswerPart1() {
    $inputs = $this->getInputs();

    /** @var Grid $grid */
    $grid = reset($inputs);

//    echo "\n" . implode('' , $grid->printMeta(metaProp: 'c')) . "\n";

    $moves = 0;
    do {
      $initial = implode('' , $grid->printMeta(metaProp: 'c'));
      $grid = $this->move($grid);
      $moves++;

      fwrite(STDERR, "\n$moves");
//      echo "\n ----- Step $moves -----";
//      echo "\n" . implode('' , $grid->printMeta(metaProp: 'c')) . "\n";

    } while (implode('' , $grid->printMeta(metaProp: 'c')) !== $initial);

    $answer = $moves;
    echo "\nAnswer: $answer";
    return $answer;
  }

  public function move(Grid $start) {
    $moved = deep_copy($start);

    foreach ($start->getPoints() as $point) {
      $c = $point->getMetaProperty('c');
      if ($c === '>') {
        $adj = $start->getAdjacentPoints($point, FALSE);

        if (!isset($adj['e'])) {
          $roll = $start->getPoints()['x0'. 'y' . $point->getY() . 'z0'];
          if ($roll->getMetaProperty('c') === '.') {
            $new = new GridCoordinate(0, $point->getY(), meta: $point->getMeta());
            $moved->movePoint($new, $point);

            $new = new GridCoordinate($point->getX(), $point->getY(), meta: ['c' => '.']);
            $moved->movePoint($new, $point);
          }
        }

        if (isset($adj['e']) && $adj['e']->getMetaProperty('c') === '.') {
          $new = new GridCoordinate($point->getX() + 1, $point->getY(), meta: $point->getMeta());
          $moved->movePoint($new, $point);

          $new = new GridCoordinate($point->getX(), $point->getY(), meta: ['c' => '.']);
          $moved->movePoint($new, $point);
        }
      }
    }

    $start = deep_copy($moved);

    foreach ($start->getPoints() as $point) {
      $c = $point->getMetaProperty('c');
      if ($c === 'v') {
        $adj = $start->getAdjacentPoints($point, FALSE);

        if (!isset($adj['n'])) {
          $roll = $start->getPoints()['x' . $point->getX() . 'y0z0'];
          if ($roll->getMetaProperty('c') === '.') {
            $new = new GridCoordinate($point->getX(), 0, meta: $point->getMeta());
            $moved->movePoint($new, $point);

            $new = new GridCoordinate($point->getX(), $point->getY(), meta: ['c' => '.']);
            $moved->movePoint($new, $point);
          }
        }

        if (isset($adj['n']) && $adj['n']->getMetaProperty('c') === '.') {
          $new = new GridCoordinate($point->getX(), $point->getY() + 1, meta: $point->getMeta());
          $moved->movePoint($new, $point);

          $new = new GridCoordinate($point->getX(), $point->getY(), meta: ['c' => '.']);
          $moved->movePoint($new, $point);
        }
      }
    }
    return $moved;
  }

  public function getAnswerPart2() {
    $inputs = $this->getInputs();

    $answer = '';
    echo "\nAnswer: $answer";
    return $answer;
  }

}
