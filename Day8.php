<?php

namespace y2021;

use y2021\src\Day;

require __DIR__ . '/../../autoload.php';

class Day8 extends Day {

  protected const DAY = 8;

  public function __construct() {
    $this->addExample(1, 1, "acedgfb cdfbe gcdfa fbcad dab cefabd cdfgeb eafb cagedb ab | cdfeb fcadb cdfeb cdbaf", "0");
    $this->addExample(1, 2, "be cfbegad cbdgef fgaecd cgeb fdcge agebfd fecdb fabcd edb | fdgacbe cefdb cefbgd gcbe\nedbfga begcd cbg gc gcadebf fbgde acbgfd abcde gfcbed gfec | fcgedb cgb dgebacf gc\nfgaebd cg bdaec gdafb agbcfd gdcbef bgcad gfac gcb cdgabef | cg cg fdcagb cbg\nfbegcd cbd adcefb dageb afcb bc aefdc ecdab fgdeca fcdbega | efabcd cedba gadfec cb\naecbfdg fbg gf bafeg dbefa fcge gcbea fcaegb dgceab fcbdga | gecf egdcabf bgf bfgea\nfgeab ca afcebg bdacfeg cfaedg gcfdb baec bfadeg bafgc acf | gebdcfa ecba ca fadegcb\ndbcfg fgd bdegcaf fgec aegbdf ecdfab fbedc dacgb gdcebf gf | cefg dcbef fcge gbcadfe\nbdfegc cbegaf gecbf dfcage bdacg ed bedf ced adcbefg gebcd | ed bcgafe cdgba cbgef\negadfb cdbfeg cegd fecab cgb gbdefca cg fgcdab egfdb bfceg | gbdfcae bgc cg cgb\ngcafb gcf dcaebfg ecagb gf abcdeg gaef cafbge fdbac fegbdc | fgae cfgab fg bagce", "26");
    $this->addExample(2, 1, "acedgfb cdfbe gcdfa fbcad dab cefabd cdfgeb eafb cagedb ab | cdfeb fcadb cdfeb cdbaf", "5353");
    $this->addExample(2, 2, "be cfbegad cbdgef fgaecd cgeb fdcge agebfd fecdb fabcd edb | fdgacbe cefdb cefbgd gcbe\nedbfga begcd cbg gc gcadebf fbgde acbgfd abcde gfcbed gfec | fcgedb cgb dgebacf gc\nfgaebd cg bdaec gdafb agbcfd gdcbef bgcad gfac gcb cdgabef | cg cg fdcagb cbg\nfbegcd cbd adcefb dageb afcb bc aefdc ecdab fgdeca fcdbega | efabcd cedba gadfec cb\naecbfdg fbg gf bafeg dbefa fcge gcbea fcaegb dgceab fcbdga | gecf egdcabf bgf bfgea\nfgeab ca afcebg bdacfeg cfaedg gcfdb baec bfadeg bafgc acf | gebdcfa ecba ca fadegcb\ndbcfg fgd bdegcaf fgec aegbdf ecdfab fbedc dacgb gdcebf gf | cefg dcbef fcge gbcadfe\nbdfegc cbegaf gecbf dfcage bdacg ed bedf ced adcbefg gebcd | ed bcgafe cdgba cbgef\negadfb cdbfeg cegd fecab cgb gbdefca cg fgcdab egfdb bfceg | gbdfcae bgc cg cgb\ngcafb gcf dcaebfg ecagb gf abcdeg gaef cafbge fdbac fegbdc | fgae cfgab fg bagce", "61229");
  }

  public function processInputs(array $inputs): array {
    $result = [];
    foreach ($inputs as $input) {
      [$signals, $outputs] = explode(' | ', $input);
      $signals = explode(' ', $signals);
      $outputs = explode(' ', $outputs);

      $all = [];

      foreach ($signals as $signal) {
        $len = strlen($signal);

        if (!isset($all[$len])) {
          $all[$len] = [$signal];
        }
        else {
          $all[$len][] = $signal;
        }
      }

      $result[] = [
        'signals' => $signals,
        'outputs' => $outputs,
        'sorted' => $all,
      ];
    }
    return $result;
  }

  public function getAnswerPart1() {
    $inputs = $this->getInputs();

    $unique = [
      1 => [],
      4 => [],
      7 => [],
      8 => [],
    ];

    foreach ($inputs as $input) {
      foreach ($input['outputs'] as $output) {
        $out = str_split($output);
        sort($out);
        $out = implode($out);
        if (strlen($output) === 2) {
          $unique[1][] = $out;
        }
        if (strlen($output) === 3) {
          $unique[7][] = $out;
        }
        if (strlen($output) === 4) {
          $unique[4][] = $out;
        }
        if (strlen($output) === 7) {
          $unique[8][] = $out;
        }
      }
    }

    $sum = 0;
    foreach ($unique as $value) {
      $sum += count($value);
    }

    $answer = $sum;
    echo "\nAnswer: $answer";
    return $answer;
  }

  public function mapWires($inputs) {
    $segments = [
      'a' => '',
      'b' => '',
      'c' => '',
      'd' => '',
      'e' => '',
      'f' => '',
      'g' => '',
    ];

    $numMap = [
      0 => [],
      1 => [],
      2 => [],
      3 => [],
      4 => [],
      5 => [],
      6 => [],
      7 => [],
      8 => [],
      9 => [],
    ];

    $numMap[1] = str_split($inputs[2][0]);
    unset($inputs[2]);

    $numMap[4] = str_split($inputs[4][0]);
    unset($inputs[4]);

    $numMap[7] = str_split($inputs[3][0]);
    unset($inputs[3]);

    $numMap[8] = str_split($inputs[7][0]);
    unset($inputs[7]);

    $this->solveForSegmentA($segments, $numMap, $inputs);
    $this->solveForSegmentG($segments, $numMap, $inputs);
    $this->solveForSegmentD($segments, $numMap, $inputs);
    $this->solveForSegmentE($segments, $numMap, $inputs);
    $this->solveForSegmentCF($segments, $numMap, $inputs);
    $this->solveForSegmentB($segments, $numMap, $inputs);

    return [
      'segMap' => $segments,
      'numMap' => $numMap
    ];
  }

  public function solveForSegmentA(&$segments, &$numMaps, &$inputs) {
    $a = array_diff($numMaps[7], $numMaps[1]);
    $segments['a'] = reset($a);
  }

  public function solveForSegmentB(&$segments, &$numMaps, &$inputs) {
    $b = array_diff($numMaps[8], $segments);
    $segments['b'] = reset($b);

    foreach ($inputs[5] as $k => $input) {
      $lit = str_split($input);
      if (in_array($segments['b'], $lit)) {
        $numMaps[5] = $lit;
        unset($inputs[5][$k]);
        $numMaps[3] = str_split(reset($inputs[5]));
        unset($inputs[5]);
        break;
      }
    }

  }

  public function solveForSegmentCF(&$segments, &$numMaps, &$inputs) {
    $c = '';
    $f = '';
    foreach ($inputs[5] as $k => $input) {
      $lit = str_split($input);
      if (count(array_diff($lit, $segments)) === 1) {
        $c = array_diff($lit, $segments);
        $numMaps[2] = $lit;
        unset($inputs[5][$k]);

        $f = array_diff($numMaps[1], $c);
        break;
      }
    }

    $segments['c'] = reset($c);
    $segments['f'] = reset($f);
  }

  public function solveForSegmentD(&$segments, &$numMaps, &$inputs) {
    $d = '';
    foreach ($inputs[6] as $k => $input) {
      $lit = str_split($input);
      $diff = array_diff($numMaps[8], $lit);
      $diff = reset($diff);
      if (!in_array($diff, $numMaps[1])) {
        $numMaps[0] = $lit;
        unset ($inputs[6][$k]);
        $d = $diff;
        $numMaps[6] = str_split(reset($inputs[6]));
        unset ($inputs[6]);
      }
    }

    $segments['d'] = $d;
  }

  public function solveForSegmentE(&$segments, &$numMaps, &$inputs) {
    $e = array_diff($numMaps[8], $numMaps[9]);

    $segments['e'] = reset($e);
  }

  public function solveForSegmentG(&$segments, &$numMaps, &$inputs) {
    $mask = array_unique(array_merge($numMaps[7], $numMaps[4]));

    foreach ($inputs[6] as $k => $input) {
      $lit = str_split($input);
      if (count(array_diff($lit,$mask)) === 1) {
        $g = array_diff($lit, $mask);
        $numMaps[9] = $lit;
        unset($inputs[6][$k]);
        break;
      }
    }

    $segments['g'] = reset($g);
  }

  public function getAnswerPart2() {
    $inputs = $this->getInputs();

    $sum = 0;
    foreach ($inputs as $input) {
      $outputs = $input['outputs'];

      $maps = $this->mapWires($input['sorted']);

      foreach ($maps['numMap'] as &$map) {
        sort($map);
        $map = implode('', $map);
      }

      $val = '';
      foreach ($outputs as $output) {
        $matched = FALSE;
        $lit = str_split($output);
        sort($lit);
        $lit = implode($lit);

        foreach ($maps['numMap'] as $num => $expected) {
          if ($lit === $expected) {
            $val .= $num;
            break;
          }
        }
      }

      $sum += (int) $val;
    }

    $answer = $sum;
    echo "\nAnswer: $answer";
    return $answer;
  }

}
