<?php

namespace y2021;

use y2021\src\Day;

require __DIR__ . '/../../autoload.php';

class Day6 extends Day {

  protected const DAY = 6;

  public function __construct() {
    $this->addExample(1, 1, "3,4,3,1,2", "5934");
    $this->addExample(2, 1, "3,4,3,1,2", "26984457539");
  }

  public function processInputs(array $inputs): array {
    $inputs = explode(',', $inputs[0]);

    $inputs = array_count_values($inputs);
    ksort($inputs);
    return $inputs;
  }

  public function getAnswerPart1() {
    $inputs = $this->getInputs();

    for ($i = 0; $i < 80; $i++) {
      $this->advanceOneDay($inputs);
    }

    $answer = array_sum($inputs);
    echo "\nAnswer: $answer";
    return $answer;
  }

  public function advanceOneDay(&$fishes) {
    $new = [];
    foreach ($fishes as $k => &$fish) {
      $d = $k-1;
      switch ($d) {
        case -1:
          $new[6] = $fish;
          $new[8] = $fish;
          break;
        default:
          if (isset($new[$d])) {
            $new[$d] += $fish;
          }
          else {
            $new[$d] = $fish;
          }
        break;
      }
    }

    ksort($new);
    $fishes = $new;
  }

  public function getAnswerPart2() {
    $inputs = $this->getInputs();

    for ($i = 0; $i < 256; $i++) {
      $this->advanceOneDay($inputs);
    }

    $answer = array_sum($inputs);
    echo "\nAnswer: $answer";
    return $answer;
  }

}
