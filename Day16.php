<?php

namespace y2021;

use y2021\src\Day;

require __DIR__ . '/../../autoload.php';

class Day16 extends Day {

  protected const DAY = 16;

  public function __construct() {
    $this->addExample(1, 1, "EE00D40C823060", "14");
    $this->addExample(1, 2, "38006F45291200", "9");
    $this->addExample(1, 3, "8A004A801A8002F478", "16");
    $this->addExample(1, 4, "620080001611562C8802118E34", "12");
    $this->addExample(1, 5, "C0015000016115A2E0802F182340", "23");
    $this->addExample(1, 6, "A0016C880162017C3686B18A3D4780", "31");
    $this->addExample(2, 1, "C200B40A82", "3");
    $this->addExample(2, 2, "04005AC33890", "54");
    $this->addExample(2, 3, "880086C3E88112", "7");
    $this->addExample(2, 4, "CE00C43D881120", "9");
    $this->addExample(2, 5, "D8005AC2A8F0", "1");
    $this->addExample(2, 6, "F600BC2D8F", "0");
    $this->addExample(2, 7, "9C005AC2F8F0", "0");
    $this->addExample(2, 8, "9C0141080250320F1802104A08", "1");
  }

  public function processInputs(array $inputs): array {
    $inputs = reset($inputs);

    $r = '';
    foreach (str_split($inputs) as $hex) {
      $b = base_convert($hex, 16, 2);
      $b = str_pad($b, 4, 0, STR_PAD_LEFT);
      $r .= $b;
    }

    return [$r];
  }

  public function getAnswerPart1() {
    $inputs = $this->getInputs();

    $inputs = reset($inputs);
    $packets = $this->parsePackets($inputs);

    $answer = $this->sumVersion($packets);
    echo "\nAnswer: $answer";
    return $answer;
  }

  public function parsePackets(&$input, $numberOfPackets = NULL) {
    $packets = [];

    $count = 0;
    while($input ) {
      if ($input == 0) {
        $this->cutString($input, strlen($input));
        break;
      }

      if ($numberOfPackets !== NULL && $count === $numberOfPackets) {
        break;
      }

      $version = bindec($this->cutString($input, 3));
      $type = bindec($this->cutString($input, 3));

      $value = '';
      switch ($type) {
        case 4:
          // Literal value.
          $last = FALSE;
          while($last === FALSE) {
            $last = $this->cutString($input, 1) === "0" ? TRUE : FALSE;
            $literal = $this->cutString($input, 4);
            $value .= $literal;
          }

          $packets[] = [
            'version' => $version,
            'type' => $type,
            'value' => bindec($value)
          ];
          break;
        default:
          // Operator.
          $lengthType = $this->cutString($input, 1);
          $packetLength = $lengthType === "1" ? 11 : 15;
          $number = bindec($this->cutString($input, $packetLength));
          if ($packetLength === 11) {
            $subpackets = $this->parsePackets($input, $number);
          }
          else {
            $fullImmediate = $this->cutString($input, $number);
            $subpackets = $this->parsePackets($fullImmediate);
          }

          $packets[] = [
            'version' => $version,
            'type' => $type,
            'packets' => $subpackets
          ];

          break;
      }

      $count++;
    }

    return $packets;
  }

  public function cutString(&$str, $length) {
    $result = substr($str, 0, $length);
    $str = substr_replace($str, '', 0, $length);

    return $result;
  }

  public function sumVersion($packets) {
    $sum = 0;
    foreach ($packets as $packet) {
      if (isset($packet['version'])) {
        $sum += $packet['version'];
      }

      if (isset($packet['packets'])) {
        $sum += $this->sumVersion($packet['packets']);
      }
    }

    return $sum;
  }

  public function getAnswerPart2() {
    $inputs = $this->getInputs();

    $inputs = reset($inputs);
    $packets = $this->parsePackets($inputs);
    foreach ($packets as $k => &$packet) {
      $this->interpretPacket($packet);
    }
    $value = reset($packets);

    $answer = $value['value'];
    echo "\nAnswer: $answer";
    return $answer;
  }

  public function interpretPacket(&$packet) {
    if (isset($packet['packets'])) {
      foreach ($packet['packets'] as &$subpacket) {
        if (isset($subpacket['packets'])) {
          $this->interpretPacket($subpacket);
        }
      }
    }

    switch ($packet['type']) {
      case 0:
        $sum = 0;
        foreach ($packet['packets'] as $p) {
          $sum += $p['value'];
        }
        $packet['value'] = $sum;
      break;
      case 1:
        $product = 1;
        foreach ($packet['packets'] as $p) {
          $product *= $p['value'];
        }
        $packet['value'] = $product;
      break;
      case 2:
        $vals = [];
        foreach ($packet['packets'] as $p) {
          $vals[] = $p['value'];
        }
        $packet['value'] = min($vals);
      break;
      case 3:
        $vals = [];
        foreach ($packet['packets'] as $p) {
          $vals[] = $p['value'];
        }
        $packet['value'] = max($vals);
      break;
      case 5:
        $packet['value'] = $packet['packets'][0]['value'] > $packet['packets'][1]['value'] ? 1 : 0;
      break;
      case 6:
        $packet['value'] = $packet['packets'][0]['value'] < $packet['packets'][1]['value'] ? 1 : 0;
      break;
      case 7:
        $packet['value'] = $packet['packets'][0]['value'] === $packet['packets'][1]['value'] ? 1 : 0;
      break;
    }
  }

}
