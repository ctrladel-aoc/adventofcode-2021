<?php

namespace y2021;

use y2021\src\Day;

require __DIR__ . '/../../autoload.php';

class Day12 extends Day {

  protected const DAY = 12;

  public function __construct() {
    $this->addExample(1, 1, "start-A\nstart-b\nA-c\nA-b\nb-d\nA-end\nb-end", "10");
    $this->addExample(1, 2, "dc-end\nHN-start\nstart-kj\ndc-start\ndc-HN\nLN-dc\nHN-end\nkj-sa\nkj-HN\nkj-dc", "19");
    $this->addExample(1, 3, "fs-end\nhe-DX\nfs-he\nstart-DX\npj-DX\nend-zg\nzg-sl\nzg-pj\npj-he\nRW-he\nfs-DX\npj-RW\nzg-RW\nstart-pj\nhe-WI\nzg-he\npj-fs\nstart-RW", "226");
    $this->addExample(2, 1, "start-A\nstart-b\nA-c\nA-b\nb-d\nA-end\nb-end", "36");
  }

  public function processInputs(array $inputs): array {
    $connections = [];
    foreach ($inputs as &$input) {
      [$start, $dest] = explode('-', $input);

      if (!isset($connections[$start])) {
        $connections[$start] = [];
      }

      if (!isset($connections[$dest])) {
        $connections[$dest] = [];
      }

      $connections[$start][] = $dest;
      $connections[$dest][] = $start;
    }

    return $connections;
  }

  public function getAnswerPart1() {
    $inputs = $this->getInputs();

    $paths = $this->findNext('start', $inputs);

    $answer = count($paths);
    echo "\nAnswer: $answer";
    return $answer;
  }

  public function findNext($current, $map, $currentPath = [], $lowerVisitLimit = 1) {
    $paths = [];
    $currentPath[] = $current;
    foreach ($map[$current] as $next) {
      if ($next === 'start') {
        continue;
      }

      $limitHit = FALSE;
      $count = array_count_values($currentPath);
      if ($lowerVisitLimit !== 1) {
        foreach ($count as $k => $v) {
          if (ctype_lower($k) && $v === $lowerVisitLimit) {
            $limitHit = TRUE;
          }
        }
      }
      $visited = $count[$next] ?? 0;

      if (isset($map[$next]) && (ctype_upper($next) || $visited < 1 || $limitHit == FALSE && $visited < $lowerVisitLimit)) {
        if ($next !== 'end') {
          $paths = array_merge($this->findNext($next, $map, $currentPath, $lowerVisitLimit), $paths);
        }
        else {
          $fullPath = $currentPath;
          $fullPath[] = $next;
          $p = implode($fullPath);
          $paths[$p] = $p;
        }
      }
    }

    return $paths;
  }

  public function getAnswerPart2() {
    $inputs = $this->getInputs();

    $paths = $this->findNext('start', $inputs, [], 2);

    $answer = count($paths);
    echo "\nAnswer: $answer";
    return $answer;
  }

}
