<?php

namespace y2021;

use y2021\src\Day;

require __DIR__ . '/../../autoload.php';

class Day18 extends Day {

  protected const DAY = 18;

  public function __construct() {
//    $this->addExample(1, 1, "[[[[9,8],1],2],3]\n4", "[[[[0,9],2],3],4]");
//    $this->addExample(1, 2, "7\n[6,[5,[4,[3,2]]]]", "[7,[6,[5,[7,0]]]]");
//    $this->addExample(1, 3, "[6,[5,[4,[3,2]]]]\n1", "[[6,[5,[7,0]]],3]");
//    $this->addExample(1, 4, "[3,[2,[1,[7,3]]]]\n[6,[5,[4,[3,2]]]]", "[[3,[2,[8,0]]],[9,[5,[7,0]]]]");
//    $this->addExample(1, 5, "[3,[2,[8,0]]]\n[9,[5,[4,[3,2]]]]", "[[3,[2,[8,0]]],[9,[5,[7,0]]]]");
//    $this->addExample(1, 6, "[[[[4,3],4],4],[7,[[8,4],9]]]\n[1,1]", "[[[[0,7],4],[[7,8],[6,0]]],[8,1]]");
//    $this->addExample(1, 7, "[1,1]\n[2,2]\n[3,3]\n[4,4]", "[[[[1,1],[2,2]],[3,3]],[4,4]]");
//    $this->addExample(1, 8, "[1,1]\n[2,2]\n[3,3]\n[4,4]\n[5,5]", "[[[[3,0],[5,3]],[4,4]],[5,5]]");
//    $this->addExample(1, 9, "[1,1]\n[2,2]\n[3,3]\n[4,4]\n[5,5]\n[6,6]", "[[[[5,0],[7,4]],[5,5]],[6,6]]");
//    $this->addExample(1, 10, "[[[0,[4,5]],[0,0]],[[[4,5],[2,6]],[9,5]]]\n[7,[[[3,7],[4,3]],[[6,3],[8,8]]]]\n[[2,[[0,8],[3,4]]],[[[6,7],1],[7,[1,6]]]]\n[[[[2,4],7],[6,[0,5]]],[[[6,8],[2,8]],[[2,1],[4,5]]]]\n[7,[5,[[3,8],[1,4]]]]\n[[2,[2,2]],[8,[8,1]]]\n[2,9]\n[1,[[[9,3],9],[[9,0],[0,7]]]]\n[[[5,[7,4]],7],1]\n[[[[4,2],2],6],[8,7]]", "[[[[8,7],[7,7]],[[8,6],[7,7]]],[[[0,7],[6,6]],[8,7]]]");
//    $this->addExample(1, 11, "[[[0,[5,8]],[[1,7],[9,6]]],[[4,[1,2]],[[1,4],2]]]\n[[[5,[2,8]],4],[5,[[9,9],0]]]\n[6,[[[6,2],[5,6]],[[7,6],[4,7]]]]\n[[[6,[0,7]],[0,9]],[4,[9,[9,0]]]]\n[[[7,[6,4]],[3,[1,3]]],[[[5,5],1],9]]\n[[6,[[7,3],[3,2]]],[[[3,8],[5,7]],4]]\n[[[[5,4],[7,7]],8],[[8,3],8]]\n[[9,3],[[9,9],[6,[4,9]]]]\n[[2,[[7,7],7]],[[5,8],[[9,3],[0,2]]]]\n[[[[5,2],5],[8,[3,7]]],[[5,[7,5]],[4,4]]]", "[[[[6,6],[7,6]],[[7,7],[7,0]]],[[[7,7],[7,7]],[[7,8],[9,9]]]]");
    $this->addExample(1, 12, "[[1,2],[[3,4],5]]", "143");
    $this->addExample(1, 13, "[[[[0,7],4],[[7,8],[6,0]]],[8,1]]", "1384");
    $this->addExample(1, 14, "[[[[1,1],[2,2]],[3,3]],[4,4]]", "445");
    $this->addExample(1, 15, "[[[[3,0],[5,3]],[4,4]],[5,5]]", "791");
    $this->addExample(1, 16, "[[[[5,0],[7,4]],[5,5]],[6,6]]", "1137");
    $this->addExample(1, 17, "[[[[8,7],[7,7]],[[8,6],[7,7]]],[[[0,7],[6,6]],[8,7]]]", "3488");
    $this->addExample(1, 17, "[[[0,[5,8]],[[1,7],[9,6]]],[[4,[1,2]],[[1,4],2]]]\n[[[5,[2,8]],4],[5,[[9,9],0]]]\n[6,[[[6,2],[5,6]],[[7,6],[4,7]]]]\n[[[6,[0,7]],[0,9]],[4,[9,[9,0]]]]\n[[[7,[6,4]],[3,[1,3]]],[[[5,5],1],9]]\n[[6,[[7,3],[3,2]]],[[[3,8],[5,7]],4]]\n[[[[5,4],[7,7]],8],[[8,3],8]]\n[[9,3],[[9,9],[6,[4,9]]]]\n[[2,[[7,7],7]],[[5,8],[[9,3],[0,2]]]]\n[[[[5,2],5],[8,[3,7]]],[[5,[7,5]],[4,4]]]", "4140");
    $this->addExample(2, 1, "[[[0,[5,8]],[[1,7],[9,6]]],[[4,[1,2]],[[1,4],2]]]\n[[[5,[2,8]],4],[5,[[9,9],0]]]\n[6,[[[6,2],[5,6]],[[7,6],[4,7]]]]\n[[[6,[0,7]],[0,9]],[4,[9,[9,0]]]]\n[[[7,[6,4]],[3,[1,3]]],[[[5,5],1],9]]\n[[6,[[7,3],[3,2]]],[[[3,8],[5,7]],4]]\n[[[[5,4],[7,7]],8],[[8,3],8]]\n[[9,3],[[9,9],[6,[4,9]]]]\n[[2,[[7,7],7]],[[5,8],[[9,3],[0,2]]]]\n[[[[5,2],5],[8,[3,7]]],[[5,[7,5]],[4,4]]]", "3993");
  }

  public function processInputs(array $inputs): array {

    return $inputs;
  }

  public function getAnswerPart1() {
    $inputs = $this->getInputs();

    foreach ($inputs as $k => &$input) {
      if ($k === 0) {
        continue;
      }
      $input = $this->snaillAdd($inputs[$k-1], $input);
    }

    $result = array_pop($inputs);
    $result = $this->parseNumbers($result);
    $magnitude = $this->findMagnitude($result);

    $answer = $magnitude;
    echo "\nAnswer: $answer";
    return $answer;
  }

  public function snaillAdd($left, $right) {
    $result = "[$left,$right]";

    $count = 0;
    do {
      $count++;
      $open = [];
      $nums = [];
      $num = '';
      $actions = [
        'explode' => [],
        'split' => [],
      ];
      $reduced = FALSE;

      for ($i = 0; $i < strlen($result); $i++) {
        $char = $result[$i];

        if ($char === '[') {
          $open[] = $i;
        }

        if ($num !== '' && in_array($char, [',', ']'])) {
          $numPos = $i - strlen($num);

          $nums[$numPos] = $num;

          if (count($open) <= 4 && $num >= 10) {
            $actions['split'][] = [
              'action' => 'split',
              'numPos' => $numPos,
              'num' => $num,
            ];
          }

          $num = '';
        }

        if ($char === ']') {
          if (count($open) > 4 && !$actions['explode']) {
            $rightPos = array_key_last($nums);
            $rightNum = array_pop($nums);
            $leftPos = array_key_last($nums);
            $leftNum = array_pop($nums);

            $actions['explode'][] = [
              'action' => 'explode',
              'leftPos' => $leftPos,
              'leftNum' => $leftNum,
              'rightPos' => $rightPos,
              'rightNum' => $rightNum,
            ];
          }
          array_pop($open);
        }

        if (!in_array($char, [',', '[', ']'])) {
          $num .= $char;
        }
      }

      if ($actions && $actions['explode']) {
        $action = reset($actions['explode']);
        $this->explode($result, $nums, $action['leftPos'], $action['leftNum'], $action['rightPos'], $action['rightNum']);
        $reduced = TRUE;
      }
      else if ($actions && $actions['split']) {
        $action = reset($actions['split']);
        $this->split($result, $action['numPos'], $action['num']);
        $reduced = TRUE;
      }

    } while($reduced);

    return $result;
  }

  public function parseNumbers($number) {
    $open = 0;
    $start = FALSE;
    $result = [];

    if (strpos($number, '[') === FALSE) {
      return explode(',', $number);
    }

    do {
      foreach (str_split($number) as $k => $c) {
        if ($c === '[') {
          $open++;

          if ($start === FALSE) {
            $start = $k;
          }
        }
        if ($c === ']') {
          $open--;
        }

        if ($start !== FALSE && $open === 0) {
          break;
        }
      }

      $s = substr($number, $start + 1, ($k) - ($start + 1));
      $r = substr($number, $start, $k - $start + 1);
      $result[] = $this->parseNumbers($s);
      $number = substr_replace($number, '', $start, strlen($r));
      $open = 0;
      $start = FALSE;
    } while(strpos($number, '[') !== FALSE);

    if (strlen($number) > 1 && strpos($number, ',') !== FALSE) {
      if (strpos($number, ',') > 0) {
        $n = explode(',', $number);
        $n = reset($n);
        array_unshift($result,$n);
      }
      if (strpos($number, ',') === 0) {
        $n = explode(',', $number);
        $n = array_pop($n);
        $result[]= $n;
      }
    }

    return $result;
  }

  public function explode(&$input, $nums, $leftPos, $leftNum, $rightPos, $rightNum) {
    $left = FALSE;
    $leftReplace = FALSE;
    for ($li = $leftPos; $li > 0; $li--) {
      if (isset($nums[$li])) {
        $leftReplace = $li;
        $left = $nums[$li] + $leftNum;
        break;
      }
    }

    $right = FALSE;
    $rightReplace = FALSE;
    for ($ri = $rightPos; $ri < strlen($input); $ri++) {
      if (isset($nums[$ri])) {
        $rightReplace = $ri;
        $right = $nums[$ri] + $rightNum;
        break;
      }
    }

    $replaceString = str_repeat('_', strlen("[$leftNum,$rightNum]"));
    $input = substr_replace($input, $replaceString, $leftPos-1, strlen($replaceString));

    if ($rightReplace) {
      $input = substr_replace($input, $right, $rightReplace, strlen($nums[$rightReplace]));
    }

    if ($leftReplace) {
      $input = substr_replace($input, $left, $leftReplace, strlen($nums[$leftReplace]));
    }

    $input = str_replace($replaceString, '0', $input);
  }

  public function split(&$input, $numPos, $num) {
    $left = floor($num/2);
    $right = ceil($num/2);
    $replace = "[$left,$right]";

    $input = substr_replace($input, $replace, $numPos, strlen($num));
  }

  public function findMagnitude(&$input) {
    foreach($input as &$c) {
      if (!is_array($c)) {
        continue;
      }

      if (is_array($c[0])) {
        $c[0] = $this->findMagnitude($c[0]);
      }

      if (is_array($c[1])) {
        $c[1] = $this->findMagnitude($c[1]);
      }

      $c = $c[0] * 3 + $c[1] * 2;;
    }

    if (isset($input[0]) && isset($input[1])) {
      return $input[0] * 3 + $input[1]  * 2;
    }
    else {
      return $input[0];
    }
  }

  public function getAnswerPart2() {
    $inputs = $this->getInputs();

    $combos = [];
    for($i = 0; $i < count($inputs); $i++) {
      for($j = 0; $j < count($inputs); $j++) {
        if ($j !== $i) {
          $combos[] = "$i|$j";
        }
      }
    }

    $combos = array_unique($combos);

    $maxMagnitude = 0;
    foreach ($combos as $trial) {
      [$first, $second] = explode('|', $trial);
      $add = $this->snaillAdd($inputs[$first], $inputs[$second]);
      $nums = $this->parseNumbers($add);
      $magnitude = $this->findMagnitude($nums);

      if ($magnitude > $maxMagnitude) {
        $maxMagnitude = $magnitude;
      }
    }

    $answer = $maxMagnitude;
    echo "\nAnswer: $answer";
    return $answer;
  }

}
