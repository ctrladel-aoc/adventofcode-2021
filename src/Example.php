<?php
declare(strict_types=1);

namespace y2021\src;


final class Example implements ExampleInterface {

  protected string $input = '';

  protected string $answer = '';

  protected string $part = '';

  protected string $number = '';

  protected array $args = [];

  public function __construct(string $part, string $number,string $input, string $answer, array $args) {
    $this->input = $input;
    $this->answer = $answer;
    $this->part = $part;
    $this->number = $number;
    $this->args = $args;
  }

  public function setInput(string $input) {
    $this->input = $input;
  }

  public function getInput(): string {
    return $this->input;
  }

  public function getAnswer(): string {
    return $this->answer;
  }

  public function getNumber(): string {
      return $this->part . "." . $this->number;
  }

  public function getArgs(): array {
    return $this->args;
  }

}