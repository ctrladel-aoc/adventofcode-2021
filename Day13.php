<?php

namespace y2021;

use aoc\Utility\Grid;
use aoc\Utility\GridCoordinate;
use y2021\src\Day;

require __DIR__ . '/../../autoload.php';

class Day13 extends Day {

  protected const DAY = 13;

  public function __construct() {
    $this->addExample(1, 1, "6,10\n0,14\n9,10\n0,3\n10,4\n4,11\n6,0\n6,12\n4,1\n0,13\n10,12\n3,4\n3,0\n8,4\n1,10\n2,14\n8,10\n9,0\n\nfold along y=7\nfold along x=5", "17");
    $this->addExample(2, 1, "6,10\n0,14\n9,10\n0,3\n10,4\n4,11\n6,0\n6,12\n4,1\n0,13\n10,12\n3,4\n3,0\n8,4\n1,10\n2,14\n8,10\n9,0\n\nfold along y=7\nfold along x=5", "#####\n#___#\n#___#\n#___#\n#####");
  }

  public function processInputs(array $inputs): array {
    $grid = new Grid();
    $instructions = [];
    $instrStart = FALSE;
    foreach ($inputs as $input) {
      if ($input === '') {
        $instrStart = TRUE;
        continue;
      }

      if (!$instrStart) {
        [$x, $y] = explode(',', $input);
        $point = new GridCoordinate($x, $y);
        $grid->addPoint($point);
      }
      else {
        $input = str_replace('fold along ', '', $input);
        $instructions[] = explode('=', $input);
      }
    }

    return ['grid' => $grid, 'folds' => $instructions];
  }

  public function getAnswerPart1() {
    $inputs = $this->getInputs();

    $grid = $this->foldGrid([$inputs['folds'][0]], $inputs['grid']);

    $answer = count($grid->getPoints());
    echo "\nAnswer: $answer";
    return $answer;
  }

  public function foldGrid(array $folds, Grid $grid): Grid {
    foreach ($folds as $fold) {
      $axis = $fold[0];
      $line = $fold[1];
      $foldedGrid = new Grid();

      if ($axis == 'x') {
        foreach ($grid->getPoints() as $point) {
          if ($point->getX() < $line) {
            $foldedGrid->addPoint($point);
          }
          else {
            $newX = abs($line * 2 - $point->getX());
            $point->setX($newX);
            $foldedGrid->addPoint($point);
          }
        }
      }
      else {
        foreach ($grid->getPoints() as $point) {
          if ($point->getY() < $line) {
            $foldedGrid->addPoint($point);
          }
          else {
            $newY = abs($line * 2 - $point->getY());
            $point->setY($newY);
            $foldedGrid->addPoint($point);
          }
        }
      }
      $grid = $foldedGrid;
    }

    return $grid;
  }

  public function getAnswerPart2() {
    $inputs = $this->getInputs();

    $grid = $this->foldGrid($inputs['folds'], $inputs['grid']);
    $answer = $grid->print();

    echo "\nAnswer: \n$answer";
    return $answer;
  }

}
