<?php

namespace y2021;

use y2021\src\Day;

require __DIR__ . '/../../autoload.php';

class Day4 extends Day {

  protected const DAY = 4;

  public function __construct() {
    $this->addExample(1, 1, "7,4,9,5,11,17,23,2,0,14,21,24,10,16,13,6,15,25,12,22,18,20,8,19,3,26,1\n\n22 13 17 11  0\n 8  2 23  4 24\n21  9 14 16  7\n 6 10  3 18  5\n 1 12 20 15 19\n\n 3 15  0  2 22\n 9 18 13 17  5\n19  8  7 25 23\n20 11 10 24  4\n14 21 16 12  6\n\n14 21 17 24  4\n10 16 15  9 19\n18  8 23 26 20\n22 11 13  6  5\n 2  0 12  3  7", "4512");
    $this->addExample(2, 1, "7,4,9,5,11,17,23,2,0,14,21,24,10,16,13,6,15,25,12,22,18,20,8,19,3,26,1\n\n22 13 17 11  0\n 8  2 23  4 24\n21  9 14 16  7\n 6 10  3 18  5\n 1 12 20 15 19\n\n 3 15  0  2 22\n 9 18 13 17  5\n19  8  7 25 23\n20 11 10 24  4\n14 21 16 12  6\n\n14 21 17 24  4\n10 16 15  9 19\n18  8 23 26 20\n22 11 13  6  5\n 2  0 12  3  7", "1924");
  }

  public function processInputs(array $inputs): array {
    $called = array_shift($inputs);
    $called = explode(',', $called);

    array_shift($inputs);

    $cardCount = 0;
    $cards = [];
    foreach ($inputs as $input) {
      if ($input === '') {
        $cardCount++;
      }
      else {
        $input = trim($input);
        $input = str_replace('  ', ' ', $input);
        $cards[$cardCount][] = explode(' ', $input);
      }
    }

    return [
      'called' => $called,
      'cards' => $cards,
    ];
  }

  public function getAnswerPart1() {
    $inputs = $this->getInputs();

    $marked = [];
    $called = $inputs['called'];
    $cards = $inputs['cards'];
    foreach ($called as $num) {
      $this->markCards($num, $cards, $marked);

      foreach ($marked as $cardNum => $markedNums) {
        if ($this->isWinner($markedNums)) {
          $winner = [
            'card' => $cards[$cardNum],
            'marked' => $markedNums,
          ];
          break 2;
        }
      }
    }

    $sum = 0;
    foreach ($winner['card'] as $r => $row) {
      foreach ($row as $c => $value) {
        if (!isset($winner['marked'][$r][$c])) {
          $sum += $value;
        }
      }
    }

    $answer = $sum * $num;
    echo "\nAnswer: $answer";
    return $answer;
  }

  public function markCards($called, $cards, &$marked) {
    foreach ($cards as $key => $card) {
      foreach ($card as $r => $row) {
        foreach ($row as $c => $value) {
          if ($value === $called) {
            if (!isset($marked[$key])) {
              $marked[$key] = [];
            }
            if (!isset($marked[$key][$r])) {
              $marked[$key][$r] = [];
            }
            if (!isset($marked[$key][$r][$c])) {
              $marked[$key][$r][$c] = [];
            }
            $marked[$key][$r][$c] = 1;
          }
        }
      }
    }

    return $marked;
  }

  public function isWinner($card) {
    // Count total number of entries.
    $s = 0;
    foreach ($card as $r => $row) {
      $s += array_sum($row);
    }
    if ($s < 5) {
      return FALSE;
    }

    $cols = [];
    foreach ($card as $r => $row) {
    // Check rows for winner.
      if (array_sum($row) === 5) {
        return TRUE;
      }

      // Check columns for winner.
      foreach ($row as $c => $value) {
        if (!isset($cols[$c])) {
          $cols[$c] = 0;
        }
        $cols[$c] += $value;

        if ($cols[$c] === 5) {
          return TRUE;
        }
      }
    }

    return FALSE;
  }

  public function getAnswerPart2() {
    $inputs = $this->getInputs();

    $marked = [];
    $winners = [];
    $called = $inputs['called'];
    $cards = $inputs['cards'];
    foreach ($called as $num) {
      $this->markCards($num, $cards, $marked);

      foreach ($marked as $card => $value) {
        if ($this->isWinner($value)) {
          $winners[$card] = TRUE;
        }
      }

      if (count($winners) === count($cards)) {
        $w = array_key_last($winners);
        $winner = [
          'card' => $cards[$w],
          'marked' => $marked[$w]
        ];

        break;
      }

    }

    $sum = 0;
    foreach ($winner['card'] as $r => $row) {
      foreach ($row as $c => $value) {
        if (!isset($winner['marked'][$r][$c])) {
          $sum += $value;
        }
      }
    }

    $answer = $sum * $num;
    echo "\nAnswer: $answer";
    return $answer;
  }

}
