<?php

namespace y2021;

use y2021\src\Day;

require __DIR__ . '/../../autoload.php';

class Day2 extends Day {

  protected const DAY = 2;

  public function __construct() {
    $this->addExample(1, 1, "forward 5\ndown 5\nforward 8\nup 3\ndown 8\nforward 2", "150");
    $this->addExample(2, 1, "forward 5\ndown 5\nforward 8\nup 3\ndown 8\nforward 2", "900");
  }

  public function processInputs(array $inputs): array {

    return $inputs;
  }

  public function getAnswerPart1() {
    $inputs = $this->getInputs();

    $z = 0;
    $x = 0;

    foreach ($inputs as $input) {
      [$direction, $change] = explode(' ', $input);

      switch ($direction) {
        case 'up':
          $z -= $change;
          break;
        case 'down':
          $z += $change;
          break;
        case 'forward':
          $x += $change;
          break;
      }
    }

    $answer = $z * $x;
    echo "\nAnswer: $answer";
    return $answer;
  }

  public function getAnswerPart2() {
    $inputs = $this->getInputs();

    $depth = 0;
    $x = 0;
    $aim = 0;

    foreach ($inputs as $input) {
      [$direction, $change] = explode(' ', $input);

      switch ($direction) {
        case 'up':
          $aim -= $change;
          break;
        case 'down':
          $aim += $change;
          break;
        case 'forward':
          $x += $change;
          $depth += $change * $aim;
          break;
      }
    }

    $answer = $depth * $x;
    echo "\nAnswer: $answer";
    return $answer;
  }

}
