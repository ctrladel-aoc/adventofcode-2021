<?php

namespace y2021;

use y2021\src\Day;

require __DIR__ . '/../../autoload.php';

class Day1 extends Day {

  protected const DAY = 1;

  protected int $target;

  public function __construct() {
    $this->addExample(1, 1, "199\n200\n208\n210\n200\n207\n240\n269\n260\n263", 7);
    $this->addExample(2, 1, "199\n200\n208\n210\n200\n207\n240\n269\n260\n263", 5);
  }

  public function getAnswerPart1() {
    $inputs = $this->getInputs();

    $increased = 0;
    $previous = 1000000;
    foreach ($inputs as $input) {
      if ($input > $previous) {
        $increased++;
      }

      $previous = $input;
    }

    $answer = $increased;
    echo "\nAnswer: $answer";

    return $answer;
  }

  public function getAnswerPart2() {
    $inputs = $this->getInputs();

    $increased = 0;
    foreach ($inputs as $key => $input) {

      if (!isset($inputs[$key+3]) || !isset($inputs[$key+2]) || !isset($inputs[$key+1])) {
        break;
      }

      $window1 = (int) $inputs[$key] + (int) $inputs[$key+1] + (int) $inputs[$key+2];
      $window2 = (int) $inputs[$key+1] + (int) $inputs[$key+2] + (int) $inputs[$key+3];

      if ($window2 > $window1) {
        $increased++;
      }
    }

    $answer = $increased;
    echo "\nAnswer: $answer";

    return $answer;
  }

}
