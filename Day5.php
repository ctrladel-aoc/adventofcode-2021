<?php

namespace y2021;

use aoc\Utility\Grid;
use aoc\Utility\GridCoordinate;
use y2021\src\Day;

require __DIR__ . '/../../autoload.php';

class Day5 extends Day {

  protected const DAY = 5;

  public function __construct() {
    $this->addExample(1, 1, "0,9 -> 5,9\n8,0 -> 0,8\n9,4 -> 3,4\n2,2 -> 2,1\n7,0 -> 7,4\n6,4 -> 2,0\n0,9 -> 2,9\n3,4 -> 1,4\n0,0 -> 8,8\n5,5 -> 8,2", "5");
    $this->addExample(2, 1, "0,9 -> 5,9\n8,0 -> 0,8\n9,4 -> 3,4\n2,2 -> 2,1\n7,0 -> 7,4\n6,4 -> 2,0\n0,9 -> 2,9\n3,4 -> 1,4\n0,0 -> 8,8\n5,5 -> 8,2", "12");
  }

  public function processInputs(array $inputs): array {
    foreach ($inputs as &$input) {
      [$pt1, $pt2] = explode(' -> ', $input);

      [$p1x, $p1y] = explode(',', $pt1);
      $pt1 = new GridCoordinate($p1x, $p1y);
      [$p2x, $p2y] = explode(',', $pt2);
      $pt2 = new GridCoordinate($p2x, $p2y);

      $input = [
        'pt1' => $pt1,
        'pt2' => $pt2,
      ];
    }

    return $inputs;
  }

  public function getAnswerPart1() {
    $inputs = $this->getInputs();

    $valid = [];
    foreach ($inputs as $input) {
      if ($input['pt1']->getX() === $input['pt2']->getX() || $input['pt1']->getY() === $input['pt2']->getY()) {
        $valid[] = $input;
      }
    }

    $marked = [];
    foreach ($valid as $input) {
      $this->markPoints($input['pt1'], $input['pt2'], $marked);
    }

    $sum = 0;
    foreach ($marked as $mark) {
      if ($mark > 1) {
        $sum++;
      }
    }

    $answer = $sum;
    echo "\nAnswer: $answer";
    return $answer;
  }

  public function markPoints($pt1, $pt2, &$marked) {
    $grid = new Grid();
    $points = $grid->getPointsOnLine($pt1, $pt2);

    foreach ($points as $point) {
      if (!isset($marked[$point->getCoordinateKey()])) {
        $marked[$point->getCoordinateKey()] = 0;
      }

      $marked[$point->getCoordinateKey()]++;
    }
  }

  public function getAnswerPart2() {
    $inputs = $this->getInputs();

    $marked = [];
    foreach ($inputs as $input) {
      $this->markPoints($input['pt1'], $input['pt2'], $marked);
    }

    $sum = 0;
    foreach ($marked as $mark) {
      if ($mark > 1) {
        $sum++;
      }
    }

    $answer = $sum;
    echo "\nAnswer: $answer";
    return $answer;
  }

}
