<?php

namespace y2021;

use y2021\src\Day;

require __DIR__ . '/../../autoload.php';

class DayN extends Day {

  protected const DAY = N;

  public function __construct() {
    $this->addExample(1, 1, "", "");
    $this->addExample(2, 1, "", "");
  }

  public function processInputs(array $inputs): array {

    return $inputs;
  }

  public function getAnswerPart1() {
    $inputs = $this->getInputs();

    $answer = '';
    echo "\nAnswer: $answer";
    return $answer;
  }

  public function getAnswerPart2() {
    $inputs = $this->getInputs();

    $answer = '';
    echo "\nAnswer: $answer";
    return $answer;
  }

}
