<?php

namespace y2021;

use y2021\src\Day;

require __DIR__ . '/../../autoload.php';

class Day7 extends Day {

  protected const DAY = 7;

  public function __construct() {
    $this->addExample(1, 1, "16,1,2,0,4,2,7,1,2,14", "37");
    $this->addExample(2, 1, "16,1,2,0,4,2,7,1,2,14", "168");
  }

  public function processInputs(array $inputs): array {
    $inputs = explode(',', $inputs[0]);
    $inputs = array_count_values($inputs);
    return $inputs;
  }

  public function getAnswerPart1() {
    $inputs = $this->getInputs();

    $max = max(array_keys($inputs));

    $fuel = [];

    for ($i = 0; $i <= $max; $i++) {
      $f = 0;
      foreach ($inputs as $l => $crabs) {
        $f += abs($i - $l)*$crabs;
      }
      $fuel[$i] = $f;
    }

    $answer = min($fuel);
    echo "\nAnswer: $answer";
    return $answer;
  }

  public function getAnswerPart2() {
    $inputs = $this->getInputs();

    $max = max(array_keys($inputs));

    $fuel = [];
    for($level = 0; $level <= $max; $level++) {
      $f = 0;
      foreach ($inputs as $p => $crabs) {
        $distance = abs($level - $p);
        $f += ($distance * ($distance + 1) / 2)*$crabs;
      }

      $fuel[$level] = $f;
    }

    $answer = min($fuel);
    echo "\nAnswer: $answer";
    return $answer;
  }

}
