<?php

namespace y2021;

use y2021\src\Day;

require __DIR__ . '/../../autoload.php';

class Day3 extends Day {

  protected const DAY = 3;

  public function __construct() {
    $this->addExample(1, 1, "00100\n11110\n10110\n10111\n10101\n01111\n00111\n11100\n10000\n11001\n00010\n01010", "198");
    $this->addExample(2, 1, "00100\n11110\n10110\n10111\n10101\n01111\n00111\n11100\n10000\n11001\n00010\n01010", "230");
  }

  public function processInputs(array $inputs): array {

    return $inputs;
  }

  public function getAnswerPart1() {
    $inputs = $this->getInputs();

    $tally = [];

    foreach ($inputs as $input) {
      foreach (str_split($input) as $i => $value) {
        if (!isset($tally[$i])) {
          $tally[$i] = [
            0 => 0,
            1 => 0,
          ];
        }

        $tally[$i][$value]++;
      }
    }

    $gamma = '';
    $epsilon = '';
    foreach($tally as $value) {
      if ($value[0] > $value[1]) {
        $gamma .= '0';
        $epsilon .= '1';
      }
      else {
        $gamma .= '1';
        $epsilon .= '0';
      }
    }

    $gamma = bindec($gamma);
    $epsilon = bindec($epsilon);

    $answer = $gamma * $epsilon;
    echo "\nAnswer: $answer";
    return $answer;
  }

  public function getAnswerPart2() {
    $inputs = $this->getInputs();

    $oxygenGen = $inputs;
    $co2Scrub = $inputs;

    $pos = 0;
    do {

      $sum = 0;

      foreach ($oxygenGen as $item) {
        $sum += $item[$pos];
      }

      if ($sum/count($oxygenGen) >= 0.5) {
        foreach ($oxygenGen as $k => $i) {
          if ($i[$pos] !== '1') {
            unset($oxygenGen[$k]);
          }
        }
      }
      else {
        foreach ($oxygenGen as $k => $i) {
          if ($i[$pos] !== '0') {
            unset($oxygenGen[$k]);
          }
        }
      }

      $pos++;
    }
    while (count($oxygenGen) > 1);

    $pos = 0;
    do {
      $sum = 0;

      foreach ($co2Scrub as $item) {
        $sum += $item[$pos];
      }

      if ($sum/count($co2Scrub) >= 0.5) {
        foreach ($co2Scrub as $k => $i) {
          if ($i[$pos] !== '0') {
            unset($co2Scrub[$k]);
          }
        }
      }
      else {
        foreach ($co2Scrub as $k => $i) {
          if ($i[$pos] !== '1') {
            unset($co2Scrub[$k]);
          }
        }
      }

      $pos++;
    }
    while (count($co2Scrub) > 1);

    $o = bindec(array_shift($oxygenGen));
    $c = bindec(array_shift($co2Scrub));

    $answer = $o * $c;
    echo "\nAnswer: $answer";
    return $answer;
  }

}
