<?php

namespace y2021;

use aoc\Utility\Grid;
use aoc\Utility\GridCoordinate;
use y2021\src\Day;

require __DIR__ . '/../../autoload.php';

class Day11 extends Day {

  protected const DAY = 11;

  public function __construct() {
    $this->addExample(1, 1, "5483143223\n2745854711\n5264556173\n6141336146\n6357385478\n4167524645\n2176841721\n6882881134\n4846848554\n5283751526", "1656");
    $this->addExample(2, 1, "5483143223\n2745854711\n5264556173\n6141336146\n6357385478\n4167524645\n2176841721\n6882881134\n4846848554\n5283751526", "195");
  }

  public function processInputs(array $inputs): array {
    $grid = new Grid();
    $energyLevels = [];

    foreach ($inputs as $y => &$input) {
      $input = str_split($input);

      foreach ($input as $x => $energy) {
        $point = new GridCoordinate($x, $y);
        $energyLevels[$point->getCoordinateKey()] = $energy;
        $grid->addPoint($point);
      }
    }

    return [
      'inputs' => $inputs,
      'grid' => $grid,
      'energy' => $energyLevels,
    ];
  }

  public function getAnswerPart1() {
    $inputs = $this->getInputs();

    $grid = $inputs['grid'];
    $energyLevels = $inputs['energy'];

    $flashed = 0;
    for ($i = 1; $i < 101; $i++) {
      $flashed += $this->step($grid, $energyLevels);
    }

    $answer = $flashed;
    echo "\nAnswer: $answer";
    return $answer;
  }

  public function step(Grid &$grid, &$energyLevels) {
    $flashes = 0;

    // Iterate the brightness by 1 for all octopi.
    foreach ($grid->getPoints() as $point) {
      $energyLevels[$point->getCoordinateKey()]++;
    }

    // Flash octopi until no more can flash.
    do {
      foreach ($grid->getPoints() as $point) {
        if ($energyLevels[$point->getCoordinateKey()] > 9) {
          $flashes++;
          $energyLevels[$point->getCoordinateKey()] = 0;

          foreach ($grid->getAdjacentPoints($point) as $adj) {
            if ($energyLevels[$adj->getCoordinateKey()] !== 0) {
              $energyLevels[$adj->getCoordinateKey()]++;
            }
          }
        }
      }

      // Check the grid to see if any octopi haven't flashed.
      $unflashed = FALSE;
      foreach ($energyLevels as $level) {
        if ($level > 9) {
          $unflashed = TRUE;
          break;
        }
      }
    } while($unflashed);

    return $flashes;
  }

  public function getAnswerPart2() {
    $inputs = $this->getInputs();

    $grid = $inputs['grid'];
    $energyLevels = $inputs['energy'];

    $step = 0;
    do {
      $step++;
      $flashed = $this->step($grid, $energyLevels);
    } while($flashed !== count($grid->getPoints()));

    $answer = $step;
    echo "\nAnswer: $answer";
    return $answer;
  }

}
