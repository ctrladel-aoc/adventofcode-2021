<?php

namespace y2021;

use y2021\src\Day;

require __DIR__ . '/../../autoload.php';

class Day21 extends Day {

  protected const DAY = 21;

  public function __construct() {
    $this->addExample(1, 1, "Player 1 starting position: 4\nPlayer 2 starting position: 8", "739785");
    $this->addExample(2, 1, "Player 1 starting position: 4\nPlayer 2 starting position: 8", "444356092776315");
  }

  public function processInputs(array $inputs): array {

    $players = [];
    foreach ($inputs as $k => $input) {
      [$p, $n, $s, $p2, $pos] = explode(' ', $input);

      $players[$n] = $pos;
    }

    return $players;
  }

  public function getAnswerPart1() {
    $inputs = $this->getInputs();

    $scores = [
      1 => 0,
      2 => 0,
    ];

    $currentPlayer = 1;
    $move = 0;
    $rolls = 0;
    do {
      for ($i = 0; $i < 3; $i++) {
        $rolls++;
        $move += $rolls%100 === 0 ? 100 : $rolls%100;
      }
      $pos = $inputs[$currentPlayer] + $move;
      $inputs[$currentPlayer] = $pos%10 === 0 ? 10 : $pos%10;
      $scores[$currentPlayer] += $inputs[$currentPlayer];
      $move = 0;

      $currentPlayer = $currentPlayer === 1 ? 2 : 1;

    }while(max($scores) < 1000);

    $answer = min($scores) * $rolls;
    echo "\nAnswer: $answer";
    return $answer;
  }

  public function getAnswerPart2() {
    $inputs = $this->getInputs();

    $wins = [
      1 => 0,
      2 => 0,
    ];
    $player1Start = $inputs[1];
    $player2Start = $inputs[2];

    $games["$player1Start:0:$player2Start:0"] = 1;
    $gameCount = 1;

    while ($gameCount > 0) {
      $this->playRound($gameCount, $games, $wins);
    }

    $answer = max($wins);
    echo "\nAnswer: $answer";
    return $answer;
  }

  public function playRound(&$gameCount, &$games, &$wins) {
    $states = [
      1 => [],
      2 => []
    ];

    foreach ($games as $state => $currentGames) {
      $gameCount -= $currentGames;

      [$pos1, $score1, $pos2, $score2] = explode(':' , $state);

      for ($roll1 = 1 ; $roll1 <= 3; $roll1++) {
        for ($roll2 = 1 ; $roll2 <= 3; $roll2++) {
          for ($roll3 = 1; $roll3 <= 3; $roll3++) {
            $move = $roll1 + $roll2 + $roll3;
            $pos = $pos1 + $move;
            $pos = $pos % 10 === 0 ? 10 : $pos % 10;
            $score = $score1 + $pos;

            if ($score >= 21) {
              $wins[1] += $currentGames;
            }
            else {
              $states[1]["$pos:$score:$pos2:$score2"] = ($states[1]["$pos:$score:$pos2:$score2"] ?? 0) + $currentGames;
              $gameCount += $currentGames;
            }
          }
        }
      }
    }
    $games = $states[1];

    foreach ($games as $state => $currentGames) {
      $gameCount -= $currentGames;

      [$pos1, $score1, $pos2, $score2] = explode(':' , $state);

      for ($roll1 = 1 ; $roll1 <= 3; $roll1++) {
        for ($roll2 = 1 ; $roll2 <= 3; $roll2++) {
          for ($roll3 = 1; $roll3 <= 3; $roll3++) {
            $move = $roll1 + $roll2 + $roll3;
            $pos = $pos2 + $move;
            $pos = $pos % 10 === 0 ? 10 : $pos % 10;
            $score = $score2 + $pos;

            if ($score >= 21) {
              $wins[2] += $currentGames;
            }
            else {
              $states[2]["$pos1:$score1:$pos:$score"] = ($states[2]["$pos1:$score1:$pos:$score"] ?? 0) + $currentGames;
              $gameCount += $currentGames;
            }
          }
        }
      }
    }
    $games = $states[2];
  }

}
