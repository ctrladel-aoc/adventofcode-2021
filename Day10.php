<?php

namespace y2021;

use y2021\src\Day;

require __DIR__ . '/../../autoload.php';

class Day10 extends Day {

  protected const DAY = 10;

  public function __construct() {
    $this->addExample(1, 1, "[({(<(())[]>[[{[]{<()<>>\n[(()[<>])]({[<{<<[]>>(\n{([(<{}[<>[]}>{[]{[(<()>\n(((({<>}<{<{<>}{[]{[]{}\n[[<[([]))<([[{}[[()]]]\n[{[{({}]{}}([{[{{{}}([]\n{<[[]]>}<{[{[{[]{()[[[]\n[<(<(<(<{}))><([]([]()\n<{([([[(<>()){}]>(<<{{\n<{([{{}}[<[[[<>{}]]]>[]]", "26397");
    $this->addExample(2, 1, "[({(<(())[]>[[{[]{<()<>>\n[(()[<>])]({[<{<<[]>>(\n{([(<{}[<>[]}>{[]{[(<()>\n(((({<>}<{<{<>}{[]{[]{}\n[[<[([]))<([[{}[[()]]]\n[{[{({}]{}}([{[{{{}}([]\n{<[[]]>}<{[{[{[]{()[[[]\n[<(<(<(<{}))><([]([]()\n<{([([[(<>()){}]>(<<{{\n<{([{{}}[<[[[<>{}]]]>[]]", "288957");
  }

  public function processInputs(array $inputs): array {

    return $inputs;
  }

  public function getAnswerPart1() {
    $inputs = $this->getInputs();

    $corruptVals = [];
    foreach ($inputs as $input) {
      $result = $this->determineIfCorrupt($input);

      if ($result['corrupted']) {
        $char = $result['corruptChar'];
        if (!isset($corruptVals[$char])) {
          $corruptVals[$char] = 1;
        }
        else {
          $corruptVals[$char]++;
        }
      }
    }

    $answer = $corruptVals[')'] * 3;
    $answer += $corruptVals[']'] * 57;
    $answer += $corruptVals['}'] * 1197;
    $answer += $corruptVals['>'] * 25137;
    echo "\nAnswer: $answer";
    return $answer;
  }

  public function determineIfCorrupt($line) {
    $closers = [
      '(' => ')',
      '[' => ']',
      '{' => '}',
      '<' => '>',
    ];

    $corruptChar = '';
    $leftOpen = [];
    $chars = str_split($line);
    foreach ($chars as $char) {
      if (in_array($char, $closers)) {
        $expected = array_pop($leftOpen);
        if ($expected !== $char) {
          $corruptChar = $char;
          break;
        }
      }
      else {
        $leftOpen[] = $closers[$char];
      }
    }

    return [
      'corrupted' => (bool) $corruptChar,
      'corruptChar' => $corruptChar,
      'leftOpen' => $leftOpen,
    ];
  }

  public function getAnswerPart2() {
    $inputs = $this->getInputs();

    $pointed = [];
    $points = [
      ')' => 1,
      ']' => 2,
      '}' => 3,
      '>' => 4
    ];

    foreach ($inputs as $input) {
      $result = $this->determineIfCorrupt($input);

      if ($result['corrupted']) {
        continue;
      }

      $sum = 0;

      $leftOpen = $result['leftOpen'];
      $leftOpen = array_reverse($leftOpen);
      foreach ($leftOpen as $closer) {
        $sum *= 5;
        $sum += $points[$closer];
      }

      $pointed[] = $sum;
    }

    sort($pointed);
    $middle = floor(count($pointed)/2);

    $answer = $pointed[$middle];
    echo "\nAnswer: $answer";
    return $answer;
  }

}
