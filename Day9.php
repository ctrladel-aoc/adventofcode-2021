<?php

namespace y2021;

use aoc\Utility\Grid;
use aoc\Utility\GridCoordinate;
use y2021\src\Day;

require __DIR__ . '/../../autoload.php';

class Day9 extends Day {

  protected const DAY = 9;

  public function __construct() {
    $this->addExample(1, 1, "2199943210\n3987894921\n9856789892\n8767896789\n9899965678", "15");
    $this->addExample(2, 1, "2199943210\n3987894921\n9856789892\n8767896789\n9899965678", "1134");
  }

  public function processInputs(array $inputs): array {
    $grid = new Grid();
    foreach ($inputs as $y => &$input) {
      $input = str_split($input);

      foreach ($input as $x => $depth) {
        $point = new GridCoordinate($x, $y, ['depth' => $depth]);
        $grid->addPoint($point);
      }
    }

    return [
      'input' => $inputs,
      'grid' => $grid,
    ];
  }

  public function getAnswerPart1() {
    $inputs = $this->getInputs();
    $grid = $inputs['grid'];

    $lowPoints = $this->findLowPoints($grid);

    $sum = 0;
    foreach ($lowPoints as $point) {
      $sum += 1 + $point->getMetaProperty('depth');
    }

    $answer = $sum;
    echo "\nAnswer: $answer";
    return $answer;
  }

  public function findLowPoints(Grid $grid) {
    $lowPoints = [];

    foreach ($grid->getPoints() as $point) {
      $adj = $grid->getAdjacentPoints($point, FALSE);
      $depth = $point->getMetaProperty('depth');

      $lowest = TRUE;
      foreach ($adj as $adjPoint) {
        if ($adjPoint->getMetaProperty('depth') <= $depth) {
          $lowest = FALSE;
          break;
        }
      }

      if ($lowest) {
        $lowPoints[] = $point;
      }
    }

    return $lowPoints;
  }

  public function getSurrounding(GridCoordinate $point, Grid $grid, $surrounding = []) {
    $adj = $grid->getAdjacentPoints($point, FALSE);

    foreach ($adj as $adjPoint) {
      if ($adjPoint->getMetaProperty('depth') !== "9") {
        if (!isset($surrounding[$adjPoint->getCoordinateKey()])) {
          $surrounding[$adjPoint->getCoordinateKey()] = TRUE;
          $surrounding = array_merge($surrounding, $this->getSurrounding($adjPoint, $grid, $surrounding));
        }
      }
    }

    return $surrounding;
  }

  public function getAnswerPart2() {
    $inputs = $this->getInputs();
    $grid = $inputs['grid'];

    $lowPoints = $this->findLowPoints($grid);

    foreach ($lowPoints as $k => $point) {
      $basin[$k] = count($this->getSurrounding($point, $grid));
    }

    sort($basin);
    $largest = array_splice($basin, -3);

    $mult = 0;
    foreach ($largest as $value) {
      if (!$mult) {
        $mult = $value;
      }
      else {
        $mult *= $value;
      }
    }

    $answer = $mult;
    echo "\nAnswer: $answer";
    return $answer;
  }

}
