<?php

namespace y2021;

use aoc\Utility\Grid;
use aoc\Utility\GridCoordinate;
use y2021\src\Day;

require __DIR__ . '/../../autoload.php';

class Day15 extends Day {

  protected const DAY = 15;

  public function __construct() {
    $this->addExample(1, 1, "1163751742\n1381373672\n2136511328\n3694931569\n7463417111\n1319128137\n1359912421\n3125421639\n1293138521\n2311944581", "40");
    $this->addExample(2, 1, "1163751742\n1381373672\n2136511328\n3694931569\n7463417111\n1319128137\n1359912421\n3125421639\n1293138521\n2311944581", "315");
  }

  public function processInputs(array $inputs): array {

    $grid = new Grid();
    foreach ($inputs as $y => $input) {
      $risks = str_split($input);

      foreach ($risks as $x => $level) {
        $point = new GridCoordinate($x, $y, ['risk' => $level]);
        $grid->addPoint($point);
      }
    }
    return ['inputs' => $inputs, 'grid' => $grid];
  }

  public function getAnswerPart1() {
    $inputs = $this->getInputs();

    /** @var Grid $grid */
    $grid = $inputs['grid'];

    $distances = $this->djk($grid);
    $bounding = $grid->findBoundingBox();

    $answer = $distances[$bounding['max']->getCoordinateKey()];
    echo "\nAnswer: $answer";
    return $answer;
  }

  public function djk(Grid $grid) {
    $points = $grid->getPoints();
    $distances = ['x0y0' => 0];
    $q = [];
    $w = ['x0y0' => 0];
    $unvisited = ['x0y0' => 0];

    foreach ($grid->getPoints() as $point) {
      $q[$point->getCoordinateKey()] = $point;
      $w[$point->getCoordinateKey()] = $point->getMetaProperty('risk');

      $previous[$point->getCoordinateKey()] = NULL;
    }

    while(!empty($unvisited)) {
      $next = $points[array_key_first($unvisited)];
      array_shift($unvisited);

      $nCoords = $next->getCoordinateKey();

      foreach ($grid->getAdjacentPoints($next, FALSE) as $adj) {
        $coords = $adj->getCoordinateKey();
        if (!isset($distances[$coords]) || $distances[$coords] > ($distances[$nCoords] + $w[$coords])) {
          $distances[$coords] = $distances[$nCoords] + $w[$coords];

          if (!isset($visited[$coords])) {
            $unvisited[$coords] = $distances[$nCoords] + $w[$coords];
          }
        }
      }
      $visited[$nCoords] = TRUE;
      unset($q[$nCoords]);

      asort($unvisited);
    }

    return $distances;
  }

  public function getAnswerPart2() {
    $inputs = $this->getInputs();
    /** @var Grid $grid */
    $grid = $inputs['grid'];
    $bounding = $grid->findBoundingBox();
    $maxX = $bounding['max']->getX() + 1;
    $maxY = $bounding['max']->getY() + 1;

    $risks = [];
    foreach ($grid->getPoints() as $p) {
      $risks[$p->getCoordinateKey()] = $p->getMetaProperty('risk');
    }

    $newGrid = clone $grid;

    for ($y = 0; $y < 5; $y++) {
      for ($x = 0; $x < 5; $x++) {
        $addRisk = $x || $y > 0 ? 1 : 0;
        foreach ($grid->getPoints() as $point) {
          $newX = $maxX * $x + $point->getX();
          $newY = $maxY * $y + $point->getY();
          if ($y === 0) {
            $prevX = $maxX * ($x - 1 > 0 ? $x - 1 : 0) + $point->getX();
            $prevRisk = $risks['x'. $prevX. 'y' . $point->getY()];
          }
          else {
            $prevX = $maxX * $x + $point->getX();
            $prevY = $maxY * ($y - 1 > 0 ? $y-1 : 0) + $point->getY();
            $prevRisk = $risks['x'.  $prevX . 'y' . $prevY];
          }
          $risk = $prevRisk + $addRisk <= 9 ? $prevRisk + $addRisk : 1;
          $risks['x' . $newX . 'y' . $newY] = $risk;
          $n = new GridCoordinate($newX, $newY, ['risk' => $risk]);
          $newGrid->addPoint($n);
        }
      }
    }

    $distances = $this->djk($newGrid);
    $bounding = $newGrid->findBoundingBox();

    $answer = $distances[$bounding['max']->getCoordinateKey()];
    echo "\nAnswer: $answer";
    return $answer;
  }

}
