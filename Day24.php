<?php

namespace y2021;

use y2021\src\Day;
use function Symfony\Component\String\b;

require __DIR__ . '/../../autoload.php';

class Day24 extends Day {

  protected const DAY = 24;

  protected $cache = [];

  public function __construct() {
  }

  public function processInputs(array $inputs): array {
    $instr = [];
    $c = -1;
    foreach ($inputs as &$input) {
      $i = explode(' ', $input);

      if ($i[0] === 'inp') {
        $c++;
        $instr[$c] = [[$i[0], 'w']];
      }
      else {
       $instr[$c][] = [$i[0], $i[1], $i[2]];
      }
    }
    return $instr;
  }

  public function getAnswerPart1() {
    $inputs = $this->getInputs();

    $min = 11111111111111;
    $max = 99999999999999;

    $count = 0;
    for ($i = $max; $i >= $min; $i--) {
      $count++;
      $model = (string) $i;
      if (strpos($model, '0') !== FALSE) {
        $z = strpos($model, '0');
        $model[$z-1] = $model[$z-1]-1;

        for ($z = $z; $z <= 13; $z++) {
          $model[$z] = 9;
        }
        $i = (int) $model;
        continue;
      }

      $memory = [
        'x' => 0,
        'y' => 0,
        'w' => 0,
        'z' => 0,
      ];
      foreach ($inputs as $k => $input) {
        if ($k === 3 && $memory['z'] > (pow(26, 7) - 1)) {
          $i = $this->skipBack($i, $k) - 1;
          continue 2;
        }
        if ($k === 4 && $memory['z'] > (pow(26, 6) - 1)) {
          $i = $this->skipBack($i, $k) - 1;
          continue 2;
        }
        if ($k === 5 && $memory['z'] > (pow(26, 6) - 1)) {
          $i = $this->skipBack($i, $k) - 1;
          continue 2;
        }
        if ($k === 6 && $memory['z'] > (pow(26, 6) - 1)) {
          $i = $this->skipBack($i, $k) - 1;
          continue 2;
        }
        if ($k === 7 && $memory['z'] > (pow(26, 6) - 1)) {
          $i = $this->skipBack($i, $k) - 1;
          continue 2;
        }
        if ($k === 8 && $memory['z'] > (pow(26, 5) - 1)) {
          $i = $this->skipBack($i, $k) - 1;
          continue 2;
        }
        if ($k === 9 && $memory['z'] > (pow(26, 5) - 1)) {
          $i = $this->skipBack($i, $k) - 1;
          continue 2;
        }
        if ($k === 10 && $memory['z'] > (pow(26, 4) - 1)) {
          $i = $this->skipBack($i, $k) - 1;
          continue 2;
        }
        if ($k === 11 && $memory['z'] > (pow(26, 3) - 1)) {
          $i = $this->skipBack($i, $k) - 1;
          continue 2;
        }
        if ($k === 12 && $memory['z'] > (pow(26, 2) - 1)) {
          $i = $this->skipBack($i, $k) - 1;
          continue 2;
        }
        if ($k === 13 && $memory['z'] > (pow(26, 1) - 1)) {
          $i = $this->skipBack($i, $k) - 1;
          continue 2;
        }

        $memory = $this->execute2($model[$k], $memory, $k, $inputs);
      }

      if ($memory['z'] === 0) {
        break;
      }
    }

    $answer = $model;
    echo "\nAnswer: $answer";
    return $answer;
  }

  public function skipBack($number, $pos) {
    $n = (string) $number;
    $nextPos = $n[$pos - 1];
    $n[$pos-1] = $nextPos - 1;

    for ($z = $pos; $z <= 13; $z++) {
      $n[$z] = 9;
    }

    return (int) $n;
  }

  public function skipAhead($number, $pos) {
    $n = (string) $number;
    do {
      $nextPos = $n[$pos - 1];

      if ($nextPos === '9') {
        $pos--;
      }

    } while ($nextPos === '9');

    $n[$pos-1] = $n[$pos - 1] + 1;

    for ($z = $pos; $z <= 13; $z++) {
      $n[$z] = 1;
    }

    return (int) $n;
  }

  public function execute($input, $memory, $programNum, $programs) {
    $key = "$programNum|$input|" . implode('|' , $memory);

    if (isset($this->cache[$key])) {
      return $this->cache[$key];
    }

    foreach ($programs[$programNum] as $instruction) {
      $op = $instruction[0];
      $a = $instruction[1];
      $b = $instruction[2] ?? NULL;
      switch ($op) {
        case 'inp':
          $memory[$a] = (int) $input;
          break;
        case 'add':
          $memory[$a] += ($memory[$b] ?? $b);
          break;
        case 'mul':
          $b = $memory[$b] ?? $b;
          $memory[$a] *= $b;
          break;
        case 'div':
          if ((isset ($memory[$b]) && $memory[$b] === 0) || $b === 0) {
            return FALSE;
          }
          $memory[$a] /= $memory[$b] ?? $b;
          $memory[$a] = floor($memory[$a]);
          break;
        case 'mod':
          if ((isset($memory[$a]) && $memory[$a] < 0) || (isset($memory[$b]) && $memory[$b] <= 0) || $b <= 0) {
            return FALSE;
          }
          $memory[$a] %= $memory[$b] ?? $b;
          break;
        case 'eql':
          $memory[$a] = $memory[$a] === (int) ($memory[$b] ?? $b) ? 1 : 0;
          break;
        }

      $memory[$a] = (int) $memory[$a];
    }

    $this->cache[$key] = $memory;

    return $memory;
  }

  public function execute2($input, $memory, $programNum) {
    $key = "$programNum|$input|" . implode('|' , $memory);

    if(isset($this->cache[$key])) {
      return $this->cache[$key];
    }

    $changes = [
      0 => ['z6' => 1, 'x7' => 12, 'y17' => 6],
      1 => ['z6' => 1, 'x7' => 10, 'y17' => 6],
      2 => ['z6' => 1, 'x7' => 13, 'y17' => 3],
      3 => ['z6' => 26, 'x7' => -11, 'y17' => 11],
      4 => ['z6' => 1, 'x7' => 13, 'y17' => 9],
      5 => ['z6' => 26, 'x7' => -1, 'y17' => 3],
      6 => ['z6' => 1, 'x7' => 10, 'y17' => 13],
      7 => ['z6' => 1, 'x7' => 11, 'y17' => 6],
      8 => ['z6' => 26, 'x7' => 0, 'y17' => 14],
      9 => ['z6' => 1, 'x7' => 10, 'y17' => 10],
      10 => ['z6' => 26, 'x7' => -5, 'y17' => 12],
      11 => ['z6' => 26, 'x7' => -16, 'y17' => 10],
      12 => ['z6' => 26, 'x7' => -7, 'y17' => 11],
      13 => ['z6' => 26, 'x7' => -11, 'y17' => 15],
    ];

    $memory['w'] = (int) $input;
    $w0 = (int) $input;
    $z0 = (int) $memory['z'];

    $x7 = ($z0%26 + $changes[$programNum]['x7']);
    $x8 = $x7 === $w0 ? 1 : 0;
    $x9 = $x8 === 0 ? 1 : 0;

    $z6 = (int) floor($z0/$changes[$programNum]['z6']);
    $z = (($w0 + $changes[$programNum]['y17']) * $x9) + (((25 * $x9) + 1) * $z6);
    $memory['z'] = (int) $z;

    $this->cache[$key] = $memory;

    return $memory;
  }

  public function getAnswerPart2() {
    $inputs = $this->getInputs();

    $min = 11111111111111;
    $max = 99999999999999;

    $count = 0;
    for ($i = $min; $i <= $max; $i++) {
      $count++;
      $model = (string) $i;

      if (strpos($model, '0') !== FALSE) {
        $model = str_replace('0', '1', $model);
        $i = (int) $model - 1;
        continue;
      }

      $memory = [
        'x' => 0,
        'y' => 0,
        'w' => 0,
        'z' => 0,
      ];
      foreach ($inputs as $k => $input) {
        if ($k === 3 && $memory['z'] > (pow(26, 7) - 1)) {
          $i = $this->skipAhead($i, $k) - 1;
          continue 2;
        }
        if ($k === 4 && $memory['z'] > (pow(26, 6) - 1)) {
          $i = $this->skipAhead($i, $k) - 1;
          continue 2;
        }
        if ($k === 5 && $memory['z'] > (pow(26, 6) - 1)) {
          $i = $this->skipAhead($i, $k) - 1;
          continue 2;
        }
        if ($k === 6 && $memory['z'] > (pow(26, 6) - 1)) {
          $i = $this->skipAhead($i, $k) - 1;
          continue 2;
        }
        if ($k === 7 && $memory['z'] > (pow(26, 6) - 1)) {
          $i = $this->skipAhead($i, $k) - 1;
          continue 2;
        }
        if ($k === 8 && $memory['z'] > (pow(26, 5) - 1)) {
          $i = $this->skipAhead($i, $k) - 1;
          continue 2;
        }
        if ($k === 9 && $memory['z'] > (pow(26, 5) - 1)) {
          $i = $this->skipAhead($i, $k) - 1;
          continue 2;
        }
        if ($k === 10 && $memory['z'] > (pow(26, 4) - 1)) {
          $i = $this->skipAhead($i, $k) - 1;
          continue 2;
        }
        if ($k === 11 && $memory['z'] > (pow(26, 3) - 1)) {
          $i = $this->skipAhead($i, $k) - 1;
          continue 2;
        }
        if ($k === 12 && $memory['z'] > (pow(26, 2) - 1)) {
          $i = $this->skipAhead($i, $k) - 1;
          continue 2;
        }
        if ($k === 13 && $memory['z'] > (pow(26, 1) - 1)) {
          $i = $this->skipAhead($i, $k) - 1;
          continue 2;
        }

        $memory = $this->execute2($model[$k], $memory, $k, $inputs);
      }

      if ($memory['z'] === 0) {
        break;
      }
    }

    $answer = $model;
    echo "\nAnswer: $answer";
    return $answer;
  }

}
