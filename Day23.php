<?php

namespace y2021;

use aoc\PatternChecker;
use aoc\Utility\Grid;
use aoc\Utility\GridCoordinate;
use y2021\src\Day;
use function DeepCopy\deep_copy;

require __DIR__ . '/../../autoload.php';

class Day23 extends Day {

  protected const DAY = 23;

  protected array $energyCosts = [
    'A' => 1,
    'B' => 10,
    'C' => 100,
    'D' => 1000,
  ];

  protected array $columnMap = [
    3 => 'A',
    5 => 'B',
    7 => 'C',
    9 => 'D',
  ];

  protected Grid $grid;

  public function __construct() {
    $grid = new Grid();

    $this->addExample(1, 1, "#############\n#...........#\n###B#C#B#D###\n  #A#D#C#A#\n  #########", "12521");
    $this->addExample(2, 1, "#############\n#...........#\n###B#C#B#D###\n  #A#D#C#A#\n  #########", "44169");
  }

  public function processInputs(array $inputs): array {
    $grid = new Grid();

    foreach ($inputs as $y => $input) {
      foreach (str_split($input) as $x => $value) {
        if ($value === ' ') {
          continue;
        }
        $point = new GridCoordinate($x, $y, meta: [
          'contains' => $value,
        ]);
        $grid->addPoint($point);
      }
    }
    return [$grid];
  }

  public function getAnswerPart1() {
    $inputs = $this->getInputs();
    /** @var Grid $initialState */
    $initialState = reset($inputs);
    $this->printState($initialState);

    fwrite(STDERR, "\n");
    $costs = $this->solve($initialState);

    $answer = min($costs);
    echo "\nAnswer: $answer";
    return $answer;
  }

  public function solve(Grid $state, int $moveCount = 0, int $maxCost = 1000000, int $tried = 0, array $movesSoFar = [], int $costSoFar = 0) {
    $solutions = [];
    $initialState = deep_copy($state);

//    $this->printState($initialState);
//    fwrite(STDERR, $this->printState($initialState));


    $initialMoves = $this->analyzeState($initialState);
    $movesSoFar = array_merge_recursive($movesSoFar, $initialMoves['history']);
    $initialMoves['history'] = $movesSoFar;
    $initialMoves = $this->findMoves($initialState, $initialMoves);


    $moves = $initialMoves['moves'];

    if ($initialMoves['priorityMoves']) {
      $moves = [];
      krsort($initialMoves['priorityMoves']);
      foreach ($initialMoves['priorityMoves'] as $amph => $m) {
        if ($m) {
          $moves = array_merge($m, $moves);
        }
      }
    }
//
//    if ($costSoFar === 0 ) {
//      $moves = [];
//      $moves['x9y2z0'] = ['x11y1z0'];
//    }
//    if ($costSoFar === 3000 ) {
//      $moves = [];
//      $moves['x9y3z0'] = ['x1y1z0'];
//    }
//    if ($costSoFar === 3010 ) {
//      $moves = [];
//      $moves['x7y2z0'] = ['x10y1z0'];
//    }
//    if ($costSoFar === 3050 ) {
//      $moves = [];
//      $moves['x7y3z0'] = ['x8y1z0'];
//    }
//    if ($costSoFar === 3080 ) {
//      $moves = [];
//      $moves['x7y4z0'] = ['x2y1z0'];
//    }


    foreach ($moves as $start => $dests) {
      foreach ($dests as $dest) {
        $state = deep_copy($initialState);
        $cost = $costSoFar;

        $cost += $this->moveAmph($state, $state->getPoints()[$start], $state->getPoints()[$dest]);
        $mc = $moveCount + 1;

        if ($mc > 500) {
          fwrite(STDERR, "\nMove cutoff");
          $solutions = [];
          continue;
        }

        if ($cost > 62500) {
          $solutions = [];
          continue;
        }

        if (!$this->checkRooms($state)) {
          $solutions = array_merge($solutions, $this->solve($state, $mc, $maxCost, $tried, $movesSoFar, $cost));
          if ($solutions) {
            $maxCost = min($solutions) < $maxCost ? min($solutions) : $maxCost;
            fwrite(STDERR, "\nMax cost: $maxCost");
          }
          $tried++;
        }
        else {
          $solutions[] = $cost;
        }
      }
    }

//    fwrite(STDERR, "\nTries so far: $tried");

    return $solutions;
  }

  public function analyzeState(Grid $state) {
    $analysis = [
      'canMove' => [],
      'open' => [],
      'history' => [],
    ];

    $bounds = $state->findBoundingBox();
    $height = $bounds['max']->getY();

    foreach ($state->getPoints() as $point) {
      if ($point->getMetaProperty('contains') === '.') {
        if ($point->getY() !== 1 || !in_array($point->getX(), array_keys($this->columnMap))) {
          $analysis['open'][] = $point;
        }
        continue;
      }

      if ($point->getMetaProperty('contains') !== '#') {
        $analysis['history'][$point->getCoordinateKey()][$point->getMetaProperty('contains')] = $point->getMetaProperty('contains');
      }

      // Don't attempt to move amphs that are stacked correctly.
      if (in_array($point->getX(), array_keys($this->columnMap))) {
        $x = $point->getX();
        $amph = $this->columnMap[$x];

        $belowMatch = TRUE;
        for ($y = $point->getY(); $y < $height; $y++) {
          if ($state->getPoints()['x' . $x . 'y' . $y . 'z0']->getMetaProperty('contains') !== $amph) {
            $belowMatch = FALSE;
          }
        }

        if ($belowMatch) {
          continue;
        }
      }

      if ($point->getMetaProperty('contains') !== '#') {
        $adj = $state->getAdjacentPoints($point);

        foreach ($adj as $a) {
          if ($a->getMetaProperty('contains') === '.') {
            $analysis['canMove'][$point->getCoordinateKey()] = $point->getCoordinateKey();
          }
        }
      }
    }

    return $analysis;
  }

  public function findMoves(Grid $state, $analysis) {
    $priorityMoves = [];
    $moves = [];

    $bounds = $state->findBoundingBox();
    $height = $bounds['max']->getY();

    foreach ($analysis['canMove'] as $point) {
      /** @var GridCoordinate $open */
      foreach ($analysis['open'] as $open) {
        $s = deep_copy($state);

        if ($state->getPoints()[$point]->getY() === 1 && $open->getY() === 1) {
          continue;
        }

        $keys = $analysis['history'][$open->getCoordinateKey()] ?? [];
        $keys = array_keys($keys);
        if (in_array($state->getPoints()[$point]->getMetaProperty('contains'), $keys)) {
          foreach ($this->columnMap as $x => $value) {
            if ($open->getX() !== $x) {
              continue;
            }

            if (!$open->getMetaProperty('contains') === $value) {
              continue 2;
            }
          }
        }

        if ($open->getY() > 1) {
          if ($this->columnMap[$open->getX()] !== $state->getPoints()[$point]->getMetaProperty('contains')) {
            continue;
          }

          for ($y = 2; $y < $height; $y++) {
            $coord = 'x' . $open->getX() . 'y' . $y . 'z0';
            if ($state->getPoints()[$coord]->getMetaProperty('contains') === '.') {
              continue;
            }

            if ($state->getPoints()[$coord]->getMetaProperty('contains') !== $this->columnMap[$open->getX()]) {
              continue 2;
            }
          }
        }
//          if ($open->getY() === 2 || $open->getY() === 3) {
//            if ($open->getX() === 3 && $state->getPoints()[$point]->getMetaProperty('contains') !== 'A') {
//              continue;
//            }
//            if ($open->getX() === 5 && $state->getPoints()[$point]->getMetaProperty('contains') !== 'B') {
//              continue;
//            }
//            if ($open->getX() === 7 && $state->getPoints()[$point]->getMetaProperty('contains') !== 'C') {
//              continue;
//            }
//            if ($open->getX() === 9 && $state->getPoints()[$point]->getMetaProperty('contains') !== 'D') {
//              continue;
//            }
//          }
//
//          if ($open->getY() === 2) {
//            $adj = $state->getAdjacentPoints($open);
//            if ($adj['n']->getMetaProperty('contains') !== $state->getPoints()[$point]->getMetaProperty('contains')) {
//              continue;
//            }
//          }

        $distance = $this->path($s, $s->getPoints()[$point], $s->getPoints()[$open->getCoordinateKey()]);
        if($distance === FALSE) {
          continue;
        }

        $moves[$point][$open->getCoordinateKey()] = $open->getCoordinateKey();
      }
    }

    // Find priority movements.
    $highest = [
      'A' => $height,
      'B' => $height,
      'C' => $height,
      'D' => $height,
    ];
    foreach ($this->columnMap as $x => $value) {
      for($y = 0; $y < $height; $y++) {
        if ($state->getPoints()['x' . $x . 'y' . $y . 'z0']->getMetaProperty('contains') === $value) {
          $highest[$value] = $y;
          break;
        }
      }
    }

    foreach ($moves as $start => $move) {
      foreach ($this->columnMap as $x => $value) {
        if ($state->getPoints()[$start]->getMetaProperty('contains') !== $value) {
          continue;
        }

        $coord = 'x' . $x . 'y' . $highest[$value] - 1 . 'z0';
        if (isset($move[$coord])) {
          $priorityMoves[$value][$start] = [$coord => $move[$coord]];
        }
      }
    }

    return [
      'priorityMoves' => $priorityMoves,
      'moves' => $moves,
    ];
  }

  public function moveAmph(Grid &$state, GridCoordinate &$from, GridCoordinate &$to) {
    $amph = $from->getMetaProperty('contains');
    $dist = $this->path($state, $from, $to);

    return $this->energyCosts[$amph] * $dist;
  }

  public function path(Grid &$state, GridCoordinate &$from, GridCoordinate &$to) {
    $steps = 0;

    $move = NULL;
    $adj = $state->getAdjacentPoints($from, FALSE);
    if ($from->getY() !== $to->getY()) {
      if ($from->getX() === $to->getX()) {                                  // We're in the hallway and at the correct X coord, move down into a room
        $move = $adj['n'];
      }
      elseif ($from->getY() > 1) {                                                  // Move up to the hallway
        $move = $adj['s'];
      }
    }

    if (!$move) {                                                               // Move along the hallway to the correct X coord
      if ($from->getY() !== 1) {
        $move = $adj['s'];
      }
      elseif ($from->getX() > $to->getX()) {
        $move = $adj['w'];
      }
      elseif ($from->getX() < $to->getX()) {
        $move = $adj['e'];
      }
    }

    if ($move->getMetaProperty('contains') !== '.') {
      return FALSE;
    }

    $state->getPoints()[$move->getCoordinateKey()]->setMetaProperty('contains', $from->getMetaProperty('contains'));
    $state->getPoints()[$from->getCoordinateKey()]->setMetaProperty('contains', '.');
    $steps++;

    if ($move->getCoordinateKey() !== $to->getCoordinateKey()) {
      $s = $this->path($state, $state->getPoints()[$move->getCoordinateKey()], $to);

      if ($s === FALSE) {
        return FALSE;
      }

      $steps += $s;
    }


    return $steps;
  }

  /**
   * @param \aoc\Utility\Grid $state
   */
  public function checkRooms(Grid $state) {
    $points = $state->getPoints();

    $bounds = $state->findBoundingBox();
    $height = $bounds['max']->getY();
    foreach ($this->columnMap as $x => $value) {
      for ($y = 2; $y < $height; $y++) {
        if ($points["x$x" . "y$y" . "z0"]->getMetaProperty('contains') !== $value) {
          return FALSE;
        }
      }
    }

    return TRUE;
  }

  public function printState(Grid $grid){
    return "\n" .$grid->printMeta(empty: ' ', metaProp: 'contains')[0] . "\n";
  }

  public function getAnswerPart2() {
    $inputs = $this->getInputs();

    /** @var Grid $initialState */
    $initialState = reset($inputs);

    $this->printState($initialState);

    foreach ($initialState->getPoints() as $point) {
      if ($point->getY() > 2) {
        $new = new GridCoordinate($point->getX(), $point->getY() + 2, meta: $point->getMeta());
        $initialState->movePoint($new, $point);
      }
    }

    $insert = [
      '  #D#C#B#A#',
      '  #D#B#A#C#',
    ];

    foreach ($insert as $y => $line) {
      foreach (str_split($line) as $x => $value) {
        if ($value === ' ') {
          continue;
        }
        $new = new GridCoordinate($x, $y+3, meta: ['contains' => $value]);
        $initialState->addPoint($new);
      }
    }

    $this->printState($initialState);

    fwrite(STDERR, "\n");
    $costs = $this->solve($initialState);

    $answer = min($costs);
    echo "\nAnswer: $answer";
    return $answer;
  }

}
